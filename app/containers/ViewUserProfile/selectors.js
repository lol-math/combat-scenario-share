import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the viewUserProfile state domain
 */
const selectViewUserProfileDomain = (state) => state.get('viewUserProfile', initialState);

/**
 * Other specific selectors
 */
export const makeSelectCombatScenarioSetsForUser = () => createSelector(
  selectViewUserProfileDomain,
  (viewUserState) => viewUserState.get('combatScenarioSets')
);
export const makeSelectLoading = () => createSelector(
  selectViewUserProfileDomain,
  (viewUserState) => viewUserState.get('loading')
);
