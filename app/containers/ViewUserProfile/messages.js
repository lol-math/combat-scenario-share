/*
 * ViewUserProfile Messages
 *
 * This contains all the text for the ViewUserProfile component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ViewUserProfile.header',
    defaultMessage: 'This is ViewUserProfile container !',
  },
});
