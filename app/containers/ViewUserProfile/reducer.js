/*
 *
 * ViewUserProfile reducer
 *
 */

import { fromJS } from 'immutable';
import {
  LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER, LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_SUCCESS, LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_ERROR,
} from './constants';

export const initialState = fromJS({
  error: false,
  loading: false,
  combatScenarioSets: [],
});

function viewUserProfileReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER:
      return state
        .set('error', false)
        .set('loading', true);
    case LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_SUCCESS:
      return state
        .set('error', false)
        .set('loading', false)
        .set('combatScenarioSets', action.combatScenarioSets);
    case LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default viewUserProfileReducer;
