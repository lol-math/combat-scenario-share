/*
 *
 * ViewUserProfile actions
 *
 */

import {
  DEFAULT_ACTION, LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER, LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_SUCCESS, LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_ERROR,
} from './constants';

export const loadRecentCombatScenariosForUser = (username, page) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER, username, page });
export const loadRecentCombatScenariosForUserSuccess = (combatScenarioSets) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_SUCCESS, combatScenarioSets });
export const loadRecentCombatScenariosForUserError = (error) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER_ERROR, error });

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
