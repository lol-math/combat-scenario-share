/**
 *
 * Asynchronously loads the component for ViewUserProfile
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
