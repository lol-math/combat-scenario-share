/**
 *
 * ViewUserProfile
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectCombatScenarioSetsForUser, makeSelectLoading } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import CombatScenarioSetList from 'containers/CombatScenarioSetList/Loadable';
import { loadRecentCombatScenariosForUser } from './actions';

class ViewUserProfile extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.onRequestCombatScenariosForUser(1);
  }
  onChangePage = (page) => {
    this.props.onRequestCombatScenariosForUser(page);
  }
  render() {
    return (
      <div>
        <Helmet>
          <title>{`User: ${this.props.username}`}</title>
          <meta name="description" content="User profile viewer" />
        </Helmet>
        <CombatScenarioSetList combatScenarioSets={this.props.combatScenarioSets} onChangePage={this.onChangePage} loading={this.props.loading} />
      </div>
    );
  }
}

ViewUserProfile.propTypes = {
  username: PropTypes.string.isRequired,
  onRequestCombatScenariosForUser: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  combatScenarioSets: makeSelectCombatScenarioSetsForUser(),
  loading: makeSelectLoading(),
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onRequestCombatScenariosForUser: (page) => dispatch(loadRecentCombatScenariosForUser(ownProps.username, page)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'viewUserProfile', reducer });
const withSaga = injectSaga({ key: 'viewUserProfile', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ViewUserProfile);
