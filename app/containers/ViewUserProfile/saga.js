import { call, put, all, select } from 'redux-saga/effects';
import { fromJS } from 'immutable';
import { takeOneAndBlock } from 'utils/customSagaEffects';
import request from 'utils/request';
import { url } from 'utils/server';

import { loadRecentCombatScenariosForUserSuccess, loadRecentCombatScenariosForUserError } from './actions';
import { LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER } from './constants';
import { makeSelectJwtToken } from '../App/selectors';

export function* loadRecentCombatScenariosSaga(action) {
  const jwtToken = yield select(makeSelectJwtToken());
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        ...(jwtToken ? { Authorization: `Bearer ${jwtToken}` } : {}),
      },
      body: JSON.stringify({
        query: `
          query AllCombatScenarios($name: String!, $page: Int) {
            allPeople(condition: {
              name: $name
            }) {
              nodes {
                id,
                name,
                combatscenariosByAuthorId(orderBy: CREATED_AT_DESC, first: 10, offset: $page) {
                  totalCount,
                  nodes {
                    id,
                    body,
                    championId,
                    published,
                    title,
                    createdAt,
                    personByAuthorId{
                      name
                    }
                  }
                }
              }
            }
          }`,
        variables:
        {
          name: 'M',
          page: 10 * (action.page - 1),
        },
      }),

    });
    yield put(loadRecentCombatScenariosForUserSuccess(fromJS(response.data.allPeople.nodes[0].combatscenariosByAuthorId)));
  } catch (err) {
    loadRecentCombatScenariosForUserError(err);
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield all([
    yield takeOneAndBlock(LOAD_RECENT_COMBAT_SCENARIOS_FOR_USER, loadRecentCombatScenariosSaga),
  ]);
}
