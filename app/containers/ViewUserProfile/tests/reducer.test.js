
import { fromJS } from 'immutable';
import viewUserProfileReducer from '../reducer';

describe('viewUserProfileReducer', () => {
  it('returns the initial state', () => {
    expect(viewUserProfileReducer(undefined, {})).toEqual(fromJS({}));
  });
});
