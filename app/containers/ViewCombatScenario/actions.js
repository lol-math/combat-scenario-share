/*
 *
 * ViewCombatScenario actions
 *
 */

import {
  DEFAULT_ACTION, LOAD_COMBAT_SCENARIO, LOAD_COMBAT_SCENARIO_SUCCESS, LOAD_COMBAT_SCENARIO_ERROR,
} from './constants';

export const loadCombatScenarioSet = (id) => ({
  type: LOAD_COMBAT_SCENARIO,
  id,
});

export const loadCombatScenarioSetSuccess = (combatScenarioSet) => ({
  type: LOAD_COMBAT_SCENARIO_SUCCESS,
  combatScenarioSet,
});

export const loadCombatScenarioSetError = () => ({
  type: LOAD_COMBAT_SCENARIO_ERROR,
});

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
