/*
 *
 * ViewCombatScenario constants
 *
 */

export const DEFAULT_ACTION = 'app/ViewCombatScenario/DEFAULT_ACTION';

export const LOAD_COMBAT_SCENARIO = 'app/ViewCombatScenario/LOAD_COMBAT_SCENARIO';
export const LOAD_COMBAT_SCENARIO_SUCCESS = 'app/ViewCombatScenario/LOAD_COMBAT_SCENARIO_SUCCESS';
export const LOAD_COMBAT_SCENARIO_ERROR = 'app/ViewCombatScenario/LOAD_COMBAT_SCENARIO_ERROR';
