import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the viewCombatScenario state domain
 */
const selectViewCombatScenarioDomain = (state) => state.get('viewCombatScenario', initialState);

/**
 * Other specific selectors
 */
const makeSelectAuthor = () => createSelector(
  selectViewCombatScenarioDomain,
  (viewCombatScenarioState) => viewCombatScenarioState.getIn(['personByAuthorId', 'name'])
);
export const makeSelectTitle = () => createSelector(
  selectViewCombatScenarioDomain,
  (viewCombatScenarioState) => viewCombatScenarioState.get('title')
);
export const makeSelectChampionId = () => createSelector(
  selectViewCombatScenarioDomain,
  (viewCombatScenarioState) => viewCombatScenarioState.get('championId')
);
export const makeSelectCombatScenarioSetGamePhaseSet = () => createSelector(
  selectViewCombatScenarioDomain,
  (viewCombatScenarioState) => viewCombatScenarioState.get('body')
);


/**
 * Default selector used by ViewCombatScenario
 */

const makeSelectViewCombatScenario = () => createSelector(
  selectViewCombatScenarioDomain,
  (substate) => substate.toJS()
);

export default makeSelectViewCombatScenario;
export {
  selectViewCombatScenarioDomain,
  makeSelectAuthor,
};
