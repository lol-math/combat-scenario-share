/*
 * ViewCombatScenario Messages
 *
 * This contains all the text for the ViewCombatScenario component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ViewCombatScenario.header',
    defaultMessage: 'This is ViewCombatScenario container !',
  },
  by: {
    id: 'app.containers.ViewCombatScenario.by',
    defaultMessage: 'By',
  },
});
