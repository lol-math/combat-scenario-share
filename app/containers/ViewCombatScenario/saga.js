import request from 'utils/request';
import { takeLatest } from 'redux-saga';
import { call, put, all } from 'redux-saga/effects';
import { url } from 'utils/server';
import { LOAD_COMBAT_SCENARIO } from './constants';
import { loadCombatScenarioSetSuccess } from './actions';

export function* loadCombatScenarioSaga(action) {
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `
        {
          combatscenarioById(id: ${action.id}) {
            id,
            body,
            createdAt,
            championId,
            title,
            updatedAt
            personByAuthorId{
              id,
              name
            }
          }
        }
        `,
      }),
    });
    yield put(loadCombatScenarioSetSuccess({ ...response.data.combatscenarioById, body: JSON.parse(response.data.combatscenarioById.body) }));
  } catch (err) {
    console.error(err);
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  yield all([
    takeLatest(LOAD_COMBAT_SCENARIO, loadCombatScenarioSaga),
  ]);
}
