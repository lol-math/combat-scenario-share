/**
 *
 * ViewCombatScenario
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import copy from 'copy-text-to-clipboard';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import CombatScenarioSetGamePhaseSet from 'components/CombatScenarioSetGamePhaseSet';
import { Button } from 'antd';
import globalMessages from 'containers/App/messages';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectAuthor, makeSelectTitle, makeSelectCombatScenarioSetGamePhaseSet, makeSelectChampionId } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { loadCombatScenarioSet } from './actions';

const ViewCombatScenarioCopyButton = styled(Button)`
  float: right;
`;

const Title = styled.h1`
  line-height: 1;
`;

const TitleAndAuthorWrapper = styled.div`
  margin-bottom: 20px;
`;

export class ViewCombatScenario extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.onLoadCombatScenario();
  }
  renderCombatScenarioSetGamePhaseSet() {
    if (this.props.combatScenarioSetGamePhaseSet.size > 0) { return <CombatScenarioSetGamePhaseSet {...this.props.combatScenarioSetGamePhaseSet.toJS()} />; }
    return null;
  }
  render() {
    return (
      <div>
        <Helmet>
          <title>Combat Scenario Viewer</title>
          <meta name="description" content="Combat Scenario Viewer" />
        </Helmet>
        <TitleAndAuthorWrapper>
          <ViewCombatScenarioCopyButton
            icon="copy"
            onClick={(e) => {
              copy(JSON.stringify({
                [this.props.championId]: this.props.combatScenarioSetGamePhaseSet.toJS(),
              }));
            }}
          >
            <FormattedMessage {...globalMessages.copyCodeButton} />
          </ViewCombatScenarioCopyButton>
          <Title>{this.props.title}</Title>
          <span><FormattedMessage {...messages.by} /> <Link to={`/user/${this.props.author}`}> {this.props.author}</Link></span>
        </TitleAndAuthorWrapper>
        {this.renderCombatScenarioSetGamePhaseSet()}
      </div>
    );
  }
}

ViewCombatScenario.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  onLoadCombatScenario: PropTypes.func.isRequired,
  combatScenarioSetGamePhaseSet: PropTypes.object.isRequired,
};
ViewCombatScenario.defaultProps = {
  title: 'Loading...',
};

const mapStateToProps = () => createStructuredSelector({
  author: makeSelectAuthor(),
  title: makeSelectTitle(),
  championId: makeSelectChampionId(),
  combatScenarioSetGamePhaseSet: makeSelectCombatScenarioSetGamePhaseSet(),
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onLoadCombatScenario: () => dispatch(loadCombatScenarioSet(ownProps.id)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'viewCombatScenario', reducer });
const withSaga = injectSaga({ key: 'viewCombatScenario', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ViewCombatScenario);
