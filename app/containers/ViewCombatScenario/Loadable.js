/**
 *
 * Asynchronously loads the component for ViewCombatScenario
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
