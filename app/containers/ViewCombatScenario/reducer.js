/*
 *
 * ViewCombatScenario reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, LOAD_COMBAT_SCENARIO_SUCCESS,
} from './constants';

export const initialState = fromJS({
  author: null,
  body: {},
});

function viewCombatScenarioReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOAD_COMBAT_SCENARIO_SUCCESS:
      return fromJS(
        { ...action.combatScenarioSet }
      );
    default:
      return state;
  }
}

export default viewCombatScenarioReducer;
