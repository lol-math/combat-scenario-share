
import { fromJS } from 'immutable';
import viewCombatScenarioReducer from '../reducer';

describe('viewCombatScenarioReducer', () => {
  it('returns the initial state', () => {
    expect(viewCombatScenarioReducer(undefined, {})).toEqual(fromJS({}));
  });
});
