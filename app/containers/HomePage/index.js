/**
 *
 * HomePage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import CombatScenarioSetList from 'containers/CombatScenarioSetList/Loadable';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { loadRecentCombatScenarios } from './actions';
import { makeSelectCombatScenarioSets, makeSelectLoading } from './selectors';

export class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.onRequestCombatScenarios(1);
  }
  onChangePage = (page) => {
    this.props.onRequestCombatScenarios(page);
  }
  render() {
    return (
      <div>
        <Helmet>
          <title>Home</title>
          <meta name="description" content="Description of HomePage" />
        </Helmet>
        <h1><FormattedMessage {...messages.header} /></h1>
        <p><FormattedMessage {...messages.combatScenarioShareDescription} /></p>
        <div>
          <h2><FormattedMessage {...messages.latestSharedCombatScenarios} /></h2>
          <CombatScenarioSetList combatScenarioSets={this.props.combatScenarioSets} onChangePage={this.onChangePage} loading={this.props.loading} />
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  combatScenarioSets: PropTypes.object.isRequired,
  onRequestCombatScenarios: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  // homepage: makeSelectHomePage(),
  combatScenarioSets: makeSelectCombatScenarioSets(),
  loading: makeSelectLoading(),
});

function mapDispatchToProps(dispatch) {
  return {
    onRequestCombatScenarios: (page) => dispatch(loadRecentCombatScenarios(page)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'homePage', reducer });
const withSaga = injectSaga({ key: 'homePage', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(HomePage);
