/*
 *
 * HomePage actions
 *
 */

import {
  DEFAULT_ACTION, LOAD_RECENT_COMBAT_SCENARIOS, LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS, LOAD_RECENT_COMBAT_SCENARIOS_ERROR,
} from './constants';

export const loadRecentCombatScenarios = (page) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS, page });
export const loadRecentCombatScenariosSuccess = (combatScenarioSets) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS, combatScenarioSets });
export const loadRecentCombatScenariosError = (error) => ({ type: LOAD_RECENT_COMBAT_SCENARIOS_ERROR, error });

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
