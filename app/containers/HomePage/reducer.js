/*
 *
 * HomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, LOAD_RECENT_COMBAT_SCENARIOS, LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS, LOAD_RECENT_COMBAT_SCENARIOS_ERROR,
} from './constants';

export const initialState = fromJS({
  error: false,
  loading: false,
  combatScenarioSets: {
    allCombatscenarios: {
      nodes: [],
    },
  },
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case LOAD_RECENT_COMBAT_SCENARIOS:
      return state
        .set('error', false)
        .set('loading', true);
    case LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS:
      return state
        .set('error', false)
        .set('loading', false)
        .set('combatScenarioSets', action.combatScenarioSets);
    case LOAD_RECENT_COMBAT_SCENARIOS_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default homePageReducer;
