import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the homePage state domain
 */
const selectHomePageDomain = (state) => state.get('homePage', initialState);

/**
 * Other specific selectors
 */

const makeSelectCombatScenarioSets = () => createSelector(
  selectHomePageDomain,
  (homeState) => homeState.get('combatScenarioSets', [])
);
export const makeSelectLoading = () => createSelector(
  selectHomePageDomain,
  (homeState) => homeState.get('loading')
);


export {
  selectHomePageDomain,
  makeSelectCombatScenarioSets,

};
