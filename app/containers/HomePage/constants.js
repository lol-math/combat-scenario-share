/*
 *
 * HomePage constants
 *
 */

export const LOAD_RECENT_COMBAT_SCENARIOS = 'app/CombatScenarioSetList/LOAD_RECENT_COMBAT_SCENARIOS';
export const LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS = 'app/CombatScenarioSetList/LOAD_RECENT_COMBAT_SCENARIOS_SUCCESS';
export const LOAD_RECENT_COMBAT_SCENARIOS_ERROR = 'app/CombatScenarioSetList/LOAD_RECENT_COMBAT_SCENARIOS_ERROR';

export const DEFAULT_ACTION = 'app/HomePage/DEFAULT_ACTION';
