/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.HomePage.header',
    defaultMessage: 'What is Combat Scenario Share about?',
  },
  combatScenarioShareDescription: {
    id: 'app.containers.HomePage.combatScenarioShareDescription',
    defaultMessage: 'Combat Scenario Share is a tool to share Item Optimizer Combat Scenarios. Item Optimizer is a tool for League of Legends to calculate which items are the best for a certain champion in a certain situation. This certain situation is called a Combat Scenario.',
  },
  latestSharedCombatScenarios: {
    id: 'app.containers.CombatScenarioSetList.latestSharedCombatScenarios',
    defaultMessage: 'The latest shared Combat Scenarios',
  },
});
