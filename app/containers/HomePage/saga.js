import { take, call, put, select, all } from 'redux-saga/effects';

import { takeOneAndBlock } from 'utils/customSagaEffects';
import { fromJS } from 'immutable';
import request from 'utils/request';
import { url } from 'utils/server';
import { LOAD_RECENT_COMBAT_SCENARIOS } from './constants';
import { loadRecentCombatScenariosError, loadRecentCombatScenariosSuccess } from './actions';

export function* loadRecentCombatScenariosSaga(action) {
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `
          query AllCombatscenarios($page: Int!) {
            allCombatscenarios (orderBy: CREATED_AT_DESC, first: 10, offset: $page){
              totalCount
              nodes{
                id, 
                title,
                published
                personByAuthorId{
                  name
                }
                createdAt,
                championId
              }
            }
          }
        `,
        variables:
        {
          page: 10 * (action.page - 1),
        },
      }),
    });
    yield put(loadRecentCombatScenariosSuccess(fromJS(response.data.allCombatscenarios)));
  } catch (err) {
    loadRecentCombatScenariosError(err);
  }
}
// Individual exports for testing
export default function* defaultSaga() {
  yield all([
    yield takeOneAndBlock(LOAD_RECENT_COMBAT_SCENARIOS, loadRecentCombatScenariosSaga),
  ]);
}
