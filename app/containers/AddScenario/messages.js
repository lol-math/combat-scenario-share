/*
 * AddScenario Messages
 *
 * This contains all the text for the AddScenario component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AddScenario.header',
    defaultMessage: 'This is AddScenario container !',
  },
  placeholderSelectChampion: {
    id: 'app.containers.AddScenario.placeholderSelectChampion',
    defaultMessage: 'Select a champion',
  },
  selectChampionNotFound: {
    id: 'app.containers.AddScenario.selectChampionNotFound',
    defaultMessage: 'There is no such champion',
  },
  tabNameEarlyGame: {
    id: 'app.containers.AddScenario.tabNameEarlyGame',
    defaultMessage: 'Early Game',
  },
  tabNameMidGame: {
    id: 'app.containers.AddScenario.tabNameMidGame',
    defaultMessage: 'Mid Game',
  },
  tabNameLateGame: {
    id: 'app.containers.AddScenario.tabNameLateGame',
    defaultMessage: 'Late Game',
  },
  addScenario: {
    id: 'app.containers.AddScenario.addScenario',
    defaultMessage: 'Add sub-scenario',
  },
  removeScenario: {
    id: 'app.containers.AddScenario.addScenario',
    defaultMessage: 'Remove',
  },
  copyCodeButton: {
    id: 'app.containers.AddScenario.copyCodeButton',
    defaultMessage: 'Copy Code',
  },
  importScenario: {
    id: 'app.containers.AddScenario.importScenario',
    defaultMessage: 'Import',
  },
  submitButton: {
    id: 'app.containers.AddScenario.submitButton',
    defaultMessage: 'Submit',
  },
  cancelButton: {
    id: 'app.containers.AddScenario.cancelButton',
    defaultMessage: 'Cancel',
  },
  addCombatScenarioTitle: {
    id: 'app.containers.AddScenario.addCombatScenarioTitle',
    defaultMessage: 'Add a combat scenario',
  },
  postTitleLabel: {
    id: 'app.containers.AddScenario.postTitleLabel',
    defaultMessage: 'Title',
  },
  hideCheckbox: {
    id: 'app.containers.AddScenario.hideCheckbox',
    defaultMessage: 'This combat scenario is private',
  },
  selectChampion: {
    id: 'app.containers.AddScenario.selectChampion',
    defaultMessage: 'Champion',
  },
  placeholderCombatScenarioPaste: {
    id: 'app.containers.AddScenario.placeholderCombatScenarioPaste',
    defaultMessage: 'Paste your Combat Scenario code here.',
  },
  pasteCodeTitle: {
    id: 'app.containers.AddScenario.pasteCodeTitle',
    defaultMessage: 'Paste code',
  },
});
