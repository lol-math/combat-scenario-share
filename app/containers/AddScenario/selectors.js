import { createSelector } from 'reselect';
import { initialState } from './reducer';
import { selectGlobal } from '../App/selectors';
import { fromJS } from 'immutable';

/**
 * Direct selector to the addScenario state domain
 */
const selectAddScenarioDomain = (state) => state.get('addScenario', initialState);

const makeSelectSelectedChampion = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('selectedChampion')
);

const makeSelectDefaultCombatScenariosSet = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('defaultCombatScenarios')
);

const makeSelectDefaultCombatScenarios = (championId) => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.getIn(['defaultCombatScenarios', championId])
);
export const makeSelectDefaultCombatScenariosSetLoading = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('defaultCombatScenariosLoading')
);
export const makeSelectSubmitLoading = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('submitLoading')
);

export const makeSelectChampionSpell = (index) => createSelector(
  selectGlobal, selectAddScenarioDomain,
  (globalState, addScenarioState) => globalState.getIn(['championInfo', addScenarioState.get('selectedChampion'), 'spells', index], fromJS({
    effectBurn: [
      null,
      '60/110/160/210/260',
      '0.6',
      '1.75',
      '2',
      '40/45/50/55/60',
      '9/8.5/8/7.5/7',
      '1.5',
      '0',
      '0',
      '0',
    ],
    tooltip: 'Kled throws a bear trap that deals {{ e1 }} <span class="colorFF8C00">(+{{ a1 }}) </span>physical damage and hooks onto the first enemy champion or large monster hit, granting <span class="coloree91d7">True Sight</span>. Deals 150% damage to minions passed through.<br /><br />If Kled stays near a hooked enemy for {{ e3 }} seconds, he deals {{ e1 }}<span class="colorFF8C00"> (+{{ charbonusphysical*2 }})</span> physical damage, yanks the enemy toward him, and slows the enemy by {{ e5 }}% for 1.5 seconds.<br /><br /><span class="colorFF8C00">Dismounted:</span> Becomes Pocket Pistol, a ranged ability that restores <span class="coloreae57e">courage</span>.',
    costType: 'No Cost',
    name: 'Bear Trap on a Rope',
    cooldown: [
      9,
      8.5,
      8,
      7.5,
      7,
    ],
    rangeBurn: '800',
    datavalues: {},
    vars: [
      {
        link: 'bonusattackdamage',
        coeff: 0.6,
        key: 'a1',
      },
    ],
    cost: [
      0,
      0,
      0,
      0,
      0,
    ],
    resource: 'No Cost',
    maxrank: 5,
    effect: [
      null,
      [
        60,
        110,
        160,
        210,
        260,
      ],
      [
        0.6,
        0.6,
        0.6,
        0.6,
        0.6,
      ],
      [
        1.75,
        1.75,
        1.75,
        1.75,
        1.75,
      ],
      [
        2,
        2,
        2,
        2,
        2,
      ],
      [
        40,
        45,
        50,
        55,
        60,
      ],
      [
        9,
        8.5,
        8,
        7.5,
        7,
      ],
      [
        1.5,
        1.5,
        1.5,
        1.5,
        1.5,
      ],
      [
        0,
        0,
        0,
        0,
        0,
      ],
      [
        0,
        0,
        0,
        0,
        0,
      ],
      [
        0,
        0,
        0,
        0,
        0,
      ],
    ],
    maxammo: '-1',
    leveltip: {
      label: [
        'Throw Damage',
        'Rip Damage',
        'Slow',
        'Cooldown',
      ],
      effect: [
        '{{ e1 }} -> {{ e1NL }}',
        '{{ e1 }} -> {{ e1NL }}',
        '{{ e5 }}% -> {{ e5NL }}%',
        '{{ cooldown }} -> {{ cooldownNL }}',
      ],
    },
    cooldownBurn: '9/8.5/8/7.5/7',
    id: 'KledQ',
    image: {
      full: 'KledQ.png',
      sprite: 'spell6.png',
      group: 'spell',
      x: 0,
      y: 48,
      w: 48,
      h: 48,
    },
    costBurn: '0',
    description: 'Kled throws a bear trap that damages and hooks an enemy champion. If shackled for a short duration, the target takes additional physical damage and is yanked toward Kled.<br><br>When dismounted, this ability is replaced by Pocket Pistol, a ranged gun blast that knocks back Kled and restores courage.',
    range: [
      800,
      800,
      800,
      800,
      800,
    ],
  }),
  )
);
export const makeSelectChampionPassive = () => createSelector(
  selectGlobal, selectAddScenarioDomain,
  (globalState, addScenarioState) => globalState.getIn(['championInfo', addScenarioState.get('selectedChampion'), 'passive'], fromJS({

    name: 'Skaarl, the Cowardly Lizard',
    description: 'Kled rides his trusty steed, Skaarl, who takes damage for him. When Skaarl\'s health depletes, Kled dismounts.<br><br>While dismounted, Kled\'s abilities change and he deals less damage to champions. Kled can restore Skaarl\'s courage by fighting enemies. At maximum courage, Kled remounts with a portion of Skaarl\'s health.',
    image: {
      full: 'Kled_P.png',
      sprite: 'passive1.png',
      group: 'passive',
      x: 384,
      y: 96,
      w: 48,
      h: 48,
    },

  }),
  )
);

export const makeSelectCurrentCombatScenariosSet = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('currentCombatScenariosSet')
);
export const makeSelectCurrentCombatScenarios = (gamePhase) => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.getIn(['currentCombatScenariosSet', gamePhase])
);
export const makeSelectCurrentCombatScenario = (gamePhase, index) => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.getIn(['currentCombatScenariosSet', gamePhase, index])
);
/**
 * @argument gamePhase string Early, Mid or Late
 * @argument combatScenarioNumber int
 * @argument variable the variable in question.
 */
const makeSelectCurrentCombatScenarioVariable = (gamePhase, combatScenarioNumber, variable) => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.getIn(['currentCombatScenariosSet', gamePhase, combatScenarioNumber, variable])
);
export const makeSelectCurrentCombatScenariosKeys = (gamePhase) => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.getIn(['currentCombatScenariosSet', gamePhase]).count()
);
const makeSelectTitle = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('title')
);
const makeSelectHidden = () => createSelector(
  selectAddScenarioDomain,
  (addScenarioState) => addScenarioState.get('hidden')
);


/**
 * Default selector used by AddScenario
 */

const makeSelectAddScenario = () => createSelector(
  selectAddScenarioDomain,
  (substate) => substate.toJS()
);

export default makeSelectAddScenario;
export {
  selectAddScenarioDomain,
  makeSelectSelectedChampion,
  makeSelectDefaultCombatScenarios,
  makeSelectDefaultCombatScenariosSet,
  makeSelectCurrentCombatScenarioVariable,
  makeSelectTitle,
  makeSelectHidden,
};
