/**
 *
 * AddScenario
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Select, Button, Input, Form, Checkbox, Spin, Icon } from 'antd';
import styled from 'styled-components';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { loadChampions } from 'containers/App/actions';
import { makeSelectChampionList } from 'containers/App/selectors';

import { makeSelectSelectedChampion, makeSelectDefaultCombatScenariosSet, makeSelectTitle, makeSelectHidden, makeSelectDefaultCombatScenariosSetLoading, makeSelectSubmitLoading } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { loadDefaultCombatScenarios, submitCombatScenario, selectChampion, changeTitle, changeHidden, copyScenario } from './actions';
import CombatScenarioWrapper from './CombatScenarioWrapper';
import { LabelledformItemLayout, UnlabelledFormItemLayout } from './FormLayout';

const { Option } = Select;
const ButtonWithMarginRight = styled(Button)`
margin-right: 8px;
`;

const FormItem = Form.Item; // This must be done in order to work with transform-react-inline-elements plugin.

export class AddScenario extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    // this.props.onRequireChampions();
    this.props.onRequireCombatScenarios();
  }
  render() {
    return (
      <div>
        <Helmet>
          <title>Add Scenario</title>
          <meta name="description" content="Share a new combat scenario" />
        </Helmet>
        <div>
          <h1 style={{ textAlign: 'center' }}>
            <FormattedMessage {...messages.addCombatScenarioTitle} />
          </h1>
          <Form
            layout="horizontal"
          >
            <FormItem
              {...LabelledformItemLayout}
              label={<FormattedMessage {...messages.postTitleLabel} />}
            >
              <Input
                value={this.props.title}
                onChange={(e) => this.props.onChangeTitle(e.target.value)}
              />
            </FormItem>
            <FormItem
              label={<FormattedMessage {...messages.selectChampion} />}
              {...LabelledformItemLayout}

            >
              <Spin indicator={<Icon type="loading" style={{ fontSize: 24 }} spin />} spinning={this.props.defaultCombatScenariosLoading}>
                <Select
                  showSearch
                  style={{ width: 200 }}
                  placeholder={<FormattedMessage {...messages.placeholderSelectChampion} />}
                  notFoundContent={<FormattedMessage {...messages.selectChampionNotFound} />}
                  optionFilterProp="children"
                  value={this.props.selectedChampion}
                  onSelect={this.props.onSelectChampion}
                  filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  {this.props.championList.map((champion) => (
                    <Option value={champion.id} key={champion.id}>{champion.name}</Option>
                  ))}
                </Select>
              </Spin>
            </FormItem>

            <FormItem
              {...UnlabelledFormItemLayout}
            >
              <Checkbox
                checked={this.props.hidden}
                onChange={(e) => this.props.onChangeHidden(e.target.checked)}
              ><FormattedMessage {...messages.hideCheckbox} /></Checkbox>
            </FormItem>
          </Form>
          <Spin indicator={<div />} spinning={this.props.selectedChampion === undefined} tip="A Champion must first be selected">

            <CombatScenarioWrapper />
          </Spin>
          <ButtonWithMarginRight
            type="primary"
            onClick={this.props.onSubmitCombatScenario}
            // loading={this.props.submitLoading}
          >
            <FormattedMessage {...messages.submitButton} />
          </ButtonWithMarginRight>
          <ButtonWithMarginRight
            icon="copy"
            onClick={this.props.onCopyButtonClick}
          >
            <FormattedMessage {...messages.copyCodeButton} />
          </ButtonWithMarginRight>

        </div>
      </div >
    );
  }
}

AddScenario.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onRequireChampions: PropTypes.func.isRequired,
  onRequireCombatScenarios: PropTypes.func.isRequired,
  combatScenarios: PropTypes.oneOfType([
    PropTypes.object.isRequired,
    PropTypes.bool,
  ]),
  championList: PropTypes.array.isRequired,
  onSelectChampion: PropTypes.func.isRequired,
  selectedChampion: PropTypes.string,
  currentCombatScenariosSet: PropTypes.object,
  onSubmitCombatScenario: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  onChangeTitle: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  championList: makeSelectChampionList(),
  selectedChampion: makeSelectSelectedChampion(),
  combatScenarios: makeSelectDefaultCombatScenariosSet(),
  title: makeSelectTitle(),
  hidden: makeSelectHidden(),
  defaultCombatScenariosLoading: makeSelectDefaultCombatScenariosSetLoading(),
  submitLoading: makeSelectSubmitLoading(),
});


function mapDispatchToProps(dispatch) {
  return {
    onRequireChampions: () => dispatch(loadChampions()),
    onRequireCombatScenarios: () => dispatch(loadDefaultCombatScenarios()),
    onSelectChampion: (champion) => dispatch(selectChampion(champion)),
    onSubmitCombatScenario: () => dispatch(submitCombatScenario()),
    onChangeTitle: (title) => dispatch(changeTitle(title)),
    onChangeHidden: (hidden) => dispatch(changeHidden(hidden)),
    onCopyButtonClick: () => dispatch(copyScenario()),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'addScenario', reducer });
const withSaga = injectSaga({ key: 'addScenario', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AddScenario);
