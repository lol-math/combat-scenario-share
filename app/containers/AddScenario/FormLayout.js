const labelSpan = 4;
const wrapperSpan = 16;

export const LabelledformItemLayout = {
  labelCol: {
    lg: { span: labelSpan },
  },
  wrapperCol: {
    lg: { span: wrapperSpan },
  },
};
export const UnlabelledFormItemLayout = {
  wrapperCol: {
    lg: {
      span: wrapperSpan,
      offset: labelSpan,
    },
  },
};
