/**
 *
 * AddScenario
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, Input, Checkbox, Slider, InputNumber, Collapse } from 'antd';
import styled from 'styled-components';

import Passive from 'components/DdImage/Passive';
import Spell from 'components/DdImage/Spell';
import SpellInput from 'components/SpellInput';

import { makeSelectCurrentCombatScenarioVariable, makeSelectCurrentCombatScenario, makeSelectSelectedChampion, makeSelectChampionSpell, makeSelectChampionPassive } from './selectors';
import { changeCombatScenarioSpecifics, removeCombatScenario, addCombatScenario } from './actions';
import { makeSelectChampionInfoSpecific } from 'containers/App/selectors';
import { UnlabelledFormItemLayout, LabelledformItemLayout } from './FormLayout';
import { FormattedMessage } from 'react-intl';
import messages from './messages';
import { makeSelectChampionsInfo } from '../App/selectors';

const AbilitiesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const FloatedRight = styled.div`
  float: right;
  margin-right: 20px;
`;
const FormItem = Form.Item; // This must be done in order to work with transform-react-inline-elements plugin.

const UnlabelledFormItem = (props) => (
  <FormItem
    {...props}
    {...UnlabelledFormItemLayout}
  />
);
const LabelledFormItem = (props) => (
  <FormItem
    {...props}
    {...LabelledformItemLayout}
  />
);

class CombatScenario extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  getSpell(num) {
    return this.props.championsInfo && this.props.championId ? this.props.championsInfo[this.props.championId].spells[num] : {};
  }
  render() {
    return (
      <Collapse.Panel
        {...this.props}
        key={this.props.index} // eslint-disable-line react/no-array-index-key
        header={
          <div>
            <span>{this.props.combatScenario.get('Title')} | {this.props.combatScenario.get('Value')}%</span>
            <FloatedRight>
              <a
                onClick={(e) => {
                  e.preventDefault();
                  this.props.onRemoveScenario(this.props.gamePhase, this.props.combatScenarioNumber);
                }}
                role="button"
                tabIndex={0}
              ><FormattedMessage {...messages.removeScenario} /></a>
            </FloatedRight>
          </div>
        }
      >
        <Form
          layout="horizontal"
        >
          <LabelledFormItem
            label="Title"
          >

            <h3>
              <Input
                name="Title"
                value={this.props.combatScenario.get('Title')}
                onChange={(e) => this.props.onChange('Title', e.target.value)}
              />
            </h3>
          </LabelledFormItem>
          <UnlabelledFormItem >
            <Checkbox
              checked={this.props.combatScenario.get('ActivateItems')}
              onChange={(e) => this.props.onChange('ActivateItems', e.target.checked)}
            >Activate Items
            </Checkbox>
          </UnlabelledFormItem>
          <UnlabelledFormItem >
            <Checkbox
              onChange={(e) => this.props.onChange('IgnoreCDR', e.target.checked)}
              checked={this.props.ignoreCDR}
            >Burst Attack
            </Checkbox>
          </UnlabelledFormItem>
          <UnlabelledFormItem >
            <Checkbox
              onChange={(e) => this.props.onChange('IgnoreLowRangeItems', e.target.checked)}
              checked={this.props.ignoreLowRangeItems}
            >
              Ranged Attack
            </Checkbox>
          </UnlabelledFormItem>
          <UnlabelledFormItem >
            <Checkbox
              onChange={(e) => this.props.onChange('LiandryDoubleDamage', e.target.checked)}
              checked={this.props.liandryDoubleDamage}
            >
              Does Liandry&apos;s deal double damage?
            </Checkbox>

          </UnlabelledFormItem>
          <LabelledFormItem
            label="Number of Targets"
          >
            <Slider
              min={1.0}
              max={5.0}
              marks={{
                1: 1, 2: 2, 3: 3, 4: 4, 5: 5,
              }}
              step={0.5}
              value={this.props.numberOfTargets}
              onChange={(value) => this.props.onChange('NumberOfTargets', value)}
            />
          </LabelledFormItem>
          <LabelledFormItem
            label="Weight"
          >
            <Slider
              min={0}
              max={100}
              step={5}
              value={this.props.value}
              marks={{
                0: 0,
                20: 20,
                40: 40,
                60: 60,
                80: 80,
                100: 100,
              }}
              onChange={(value) => this.props.onChange('Value', value)}
            />
          </LabelledFormItem>
          <LabelledFormItem
            label="Combat Duration"
          >
            <InputNumber
              onChange={(value) => this.props.onChange('Duration', value)}
              value={this.props.duration}
              step={0.5}
            />
          </LabelledFormItem>
          <LabelledFormItem
            label="Sheen Procs"
          >
            <InputNumber
              onChange={(value) => this.props.onChange('S', value)}
              value={this.props.s}
              step={0.5}
            />
          </LabelledFormItem>
          <LabelledFormItem
            label="Basic attacks"
          >
            <InputNumber
              onChange={(value) => this.props.onChange('B', value)}
              value={this.props.b}
              step={0.5}
            />
          </LabelledFormItem>
          <LabelledFormItem
            label="Basic Attack Seconds"
          >
            <Slider
              min={0}
              max={this.props.duration}
              step={0.5}
              value={this.props.c}
              onChange={(value) => this.props.onChange('C', value)}
              marks={{
                0: 0,
                [this.props.duration]: this.props.duration,
              }}
            />
          </LabelledFormItem>
          <UnlabelledFormItem>
            <AbilitiesWrapper >
              <SpellInput
                title="Passive"
                onChange={(value) => this.props.onChange('P', value)}
                value={this.props.p}
              >
                <Passive
                  ability={this.props.championPassive}
                />
              </SpellInput>
              <SpellInput
                title="Q"
                onChange={(value) => this.props.onChange('Q', value)}
                value={this.props.q}
              >
                <Spell
                  ability={this.props.championSpellQ}
                  size={64}
                />
              </SpellInput>
              <SpellInput
                title="W"
                onChange={(value) => this.props.onChange('W', value)}
                value={this.props.w}
              >
                <Spell
                  ability={this.props.championSpellW}
                  size={64}
                />
              </SpellInput>
              <SpellInput
                title="E"
                onChange={(value) => this.props.onChange('E', value)}
                value={this.props.e}
              >
                <Spell
                  ability={this.props.championSpellE}
                  size={64}
                />
              </SpellInput>
              <SpellInput
                title="R"
                name="R"
                onChange={(value) => this.props.onChange('R', value)}
                value={this.props.r}
              >
                <Spell
                  ability={this.props.championSpellR}
                  size={64}
                />
              </SpellInput>
            </AbilitiesWrapper>
          </UnlabelledFormItem>
        </Form>
      </Collapse.Panel>

    );
  }
}

CombatScenario.propTypes = {
  dispatch: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  activateItems: PropTypes.bool,
  ignoreCDR: PropTypes.bool,
  ignoreLowRangeItems: PropTypes.bool,
  liandryDoubleDamage: PropTypes.bool,
  numberOfTargets: PropTypes.number,
  value: PropTypes.number,
  duration: PropTypes.number,
  c: PropTypes.number,
  p: PropTypes.number,
  q: PropTypes.number,
  w: PropTypes.number,
  e: PropTypes.number,
  r: PropTypes.number,
};

const mapStateToProps = (state, ownProps) => createStructuredSelector({
  // addscenario: makeSelectAddScenario(),
  selectedChampion: makeSelectSelectedChampion(),
  combatScenario: makeSelectCurrentCombatScenario(ownProps.gamePhase, ownProps.combatScenarioNumber),
  title: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'Title'),
  activateItems: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'ActivateItems'),
  ignoreCDR: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'IgnoreCDR'),
  ignoreLowRangeItems: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'IgnoreLowRangeItems'),
  liandryDoubleDamage: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'LiandryDoubleDamage'),
  numberOfTargets: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'NumberOfTargets'),
  value: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'Value'),
  duration: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'Duration'),
  c: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'C'),
  b: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'B'),
  p: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'P'),
  q: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'Q'),
  w: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'W'),
  e: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'E'),
  r: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'R'),
  s: makeSelectCurrentCombatScenarioVariable(ownProps.gamePhase, ownProps.combatScenarioNumber, 'S'),
  championsInfo: makeSelectChampionsInfo(),
  championPassive: makeSelectChampionPassive(),
  championSpellQ: makeSelectChampionSpell(0),
  championSpellW: makeSelectChampionSpell(1),
  championSpellE: makeSelectChampionSpell(2),
  championSpellR: makeSelectChampionSpell(3),
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    onChange: (variable, val) => dispatch(changeCombatScenarioSpecifics(ownProps.gamePhase, ownProps.combatScenarioNumber, variable, val)),
    onRemoveScenario: (gamePhase, combatScenarioNumber) => dispatch(removeCombatScenario(gamePhase, combatScenarioNumber)),
    onAddScenario: (gamePhase) => dispatch(addCombatScenario(gamePhase)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(CombatScenario);
