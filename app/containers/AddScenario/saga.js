/**
 * Gets data from ddragon.
 */

import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import request from 'utils/request';
import { getChampions } from 'containers/App/saga';
import { fromJS } from 'immutable';
import copy from 'copy-text-to-clipboard';
import { url } from 'utils/server';

import { LOAD_CHAMPIONS } from 'containers/App/constants';
import { makeSelectUser, makeSelectJwtToken } from 'containers/App/selectors';
import { makeSelectDefaultCombatScenarios, makeSelectCurrentCombatScenariosSet, makeSelectSelectedChampion, makeSelectTitle, makeSelectHidden } from './selectors';
import { LOAD_COMBAT_SCENARIOS, SELECT_CHAMPION, POST_COMBAT_SCENARIO, COPY_SCENARIO, POST_COMBAT_SCENARIO_SUCCESS, IMPORT_COMBAT_SCENARIO } from './constants';
import { defaultCombatScenariosLoaded, defaultCombatScenariosLoadingError, setCurrentCombatScenario, submitCombatScenarioSuccess, submitCombatScenarioError } from './actions';
// import { makeSelectUsername } from './selectors';

export function* getCombatScenarios() {
  try {
    const combatScenarios = yield call(request, 'https://lolmath.net/data/Champion_Battle_Set.json');

    // unfix Wukong.
    combatScenarios.MonkeyKing = combatScenarios.Wukong;
    combatScenarios.Wukong = undefined;

    yield put(defaultCombatScenariosLoaded(combatScenarios));
  } catch (err) {
    yield put(defaultCombatScenariosLoadingError(err));
  }
}

export function* changeCombatScenarioSaga(action) {
  try {
    const combatScenario = yield select(makeSelectDefaultCombatScenarios(action.championId));
    yield put(setCurrentCombatScenario(combatScenario));
  } catch (err) {
    console.error(err);
  }
}

export function* submitCombatScenarioSaga() {
  try {
    const combatScenario = yield select(makeSelectCurrentCombatScenariosSet());
    const championId = yield select(makeSelectSelectedChampion());
    const title = yield select(makeSelectTitle());
    const hidden = yield select(makeSelectHidden());
    const user = yield select(makeSelectUser());
    const jwtToken = yield select(makeSelectJwtToken());

    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        query: `
          mutation CreateCombatScenario($input: CreateCombatscenarioInput!) {
            createCombatscenario(input: $input) {
              combatscenario{
                id
              }
            }
          }`,
        variables:
        {
          input: {
            combatscenario: {
              authorId: user.get('id'),
              body: JSON.stringify(combatScenario),
              championId,
              title,
              published: !hidden,
            },
          },
        },
      }),

    });
    yield put(submitCombatScenarioSuccess(response.data.createCombatscenario.combatscenario.id));
  } catch (err) {
    yield put(submitCombatScenarioError(err));
    console.error(err);
  }
}
export function* copyScenarioSaga() {
  const selectedChampion = yield select(makeSelectSelectedChampion());
  const currentCombatScenariosSet = yield select(makeSelectCurrentCombatScenariosSet());

  copy(JSON.stringify({
    [selectedChampion]: currentCombatScenariosSet.toJS(),
  }));
}
export function* importScenarioSaga(action) {
  try {
    const scenario = JSON.parse(action.scenario);
    const usableScenario = scenario[Object.keys(scenario)[0]];
    yield put(setCurrentCombatScenario(fromJS(usableScenario)));
  } catch (e) {
    console.err(e);
  }
}
export function* submitCombatScenarioSuccessSaga() {

}
/**
 * Root saga manages watcher lifecycle
 */
export default function* ddragonData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(LOAD_CHAMPIONS, getChampions);
  yield all([
    takeLatest(LOAD_CHAMPIONS, getChampions),
    takeLatest(LOAD_COMBAT_SCENARIOS, getCombatScenarios),
    takeLatest(SELECT_CHAMPION, changeCombatScenarioSaga),
    takeLatest(POST_COMBAT_SCENARIO, submitCombatScenarioSaga),
    takeLatest(POST_COMBAT_SCENARIO_SUCCESS, submitCombatScenarioSuccessSaga),
    takeLatest(COPY_SCENARIO, copyScenarioSaga),
    takeLatest(IMPORT_COMBAT_SCENARIO, importScenarioSaga),
  ]);
}
