import { FormattedMessage } from 'react-intl';
import { Tabs, Button, Modal, Input } from 'antd';
import styled from 'styled-components';
import React from 'react';

import messages from './messages';
import CombatScenarioList from './CombatScenarioList';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { importScenario } from './actions';

const TabPane = Tabs.TabPane;
const WrapperStyle = styled.div`
  margin: 5px 0;
  border: 1px solid #E8E8E8;
  border-bottom: 0;
`;

const ButtonWithMarginRight = styled(Button)`
margin-right: 4px;
`;

const { TextArea } = Input;

class CombatScenarioWrapper extends React.Component {
  state = {
    visible: false,
    combatScenario: '',
  }

  showModal = () => {
    this.setState({
      visible: true,
    });
  }

  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
    this.props.onImportCombatScenario(this.state.combatScenario);
  }
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  handleTextChange= (e) => {
    this.setState({ combatScenario: e.target.value });
  }


  render = () => (
    <WrapperStyle >
      <Modal
        title={<FormattedMessage {...messages.pasteCodeTitle} />}
        visible={this.state.visible}
        onOk={this.handleOk}
        onCancel={this.handleCancel}
        okText={<FormattedMessage {...messages.submitButton} />}
        cancelText={<FormattedMessage {...messages.cancelButton} />}
      >
        <FormattedMessage {...messages.placeholderCombatScenarioPaste}>
          {(placeholderText) => (
            <TextArea
                rows={4}
                placeholder={placeholderText}
                onChange={this.handleTextChange}
              />
            )}
        </FormattedMessage>
      </Modal>
      <Tabs
        defaultActiveKey="Early"
        tabBarStyle={{ margin: 0 }}
        tabBarExtraContent={<ButtonWithMarginRight icon="select" onClick={this.showModal}><FormattedMessage {...messages.importScenario} /></ButtonWithMarginRight>}
      >
        {
            [
              {
                tabName: <FormattedMessage {...messages.tabNameEarlyGame} />,
                key: 'Early',
              },
              {
                tabName: <FormattedMessage {...messages.tabNameMidGame} />,
                key: 'Mid',
              },
              {
                tabName: <FormattedMessage {...messages.tabNameLateGame} />,
                key: 'Late',
              },
            ].map((gamePhase) => (
              <TabPane tab={gamePhase.tabName} key={gamePhase.key}>
                <CombatScenarioList
                  gamePhase={gamePhase.key}
                />
              </TabPane>
            ))
          }
      </Tabs>
    </WrapperStyle>
    )
}

function mapDispatchToProps(dispatch) {
  return {
    onImportCombatScenario: (combatScenario) => dispatch(importScenario(combatScenario)),
    dispatch,
  };
}
const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(CombatScenarioWrapper);
