/**
 *
 * AddScenario
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Collapse, Button } from 'antd';
import styled from 'styled-components';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { makeSelectCurrentCombatScenariosKeys, makeSelectSelectedChampion } from './selectors';
import reducer from './reducer';
import saga from './saga';
// import messages from './messages';
// import { loadChampions, selectChampion } from './actions';
import CombatScenario from './CombatScenario';
import { removeCombatScenario, addCombatScenario, changeCombatScenarioSpecifics } from './actions';
import messages from './messages';

const ActionDiv = styled.div`
  padding: 5px;
  background: #fbfbfb;
`;

class CombatScenarioList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <ActionDiv>
          <Button
            icon="plus"
            onClick={(e) => {
              e.preventDefault();
              this.props.onAddScenario(this.props.gamePhase);
            }}
            size="small"
          >
            <FormattedMessage {...messages.addScenario} />
          </Button>

        </ActionDiv>
        <Collapse accordion bordered={false}>
          {Array(this.props.currentCombatScenariosKeys).fill().map((a, i) =>
            (

              <CombatScenario
                combatScenarioNumber={i}
                gamePhase={this.props.gamePhase}
              />
            ))}
        </Collapse>

      </div>
    );
  }
}

CombatScenarioList.propTypes = {
  dispatch: PropTypes.func.isRequired,
  combatScenarios: PropTypes.array,
  onAddScenario: PropTypes.func.isRequired,
  gamePhase: PropTypes.string.isRequired,
  selectedChampion: PropTypes.string.isRequired,
  onRemoveScenario: PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => createStructuredSelector({
  currentCombatScenariosKeys: makeSelectCurrentCombatScenariosKeys(ownProps.gamePhase),
  // selectedChampion: makeSelectSelectedChampion(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onRemoveScenario: (gamePhase, combatScenarioNumber) => dispatch(removeCombatScenario(gamePhase, combatScenarioNumber)),
    onAddScenario: (gamePhase) => dispatch(addCombatScenario(gamePhase)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

// const withReducer = injectReducer({ key: 'addScenario', reducer });

export default compose(
  // withReducer,
  withConnect,
)(CombatScenarioList);
