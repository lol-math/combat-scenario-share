
import { fromJS } from 'immutable';
import addScenarioReducer from '../reducer';

describe('addScenarioReducer', () => {
  it('returns the initial state', () => {
    expect(addScenarioReducer(undefined, {})).toEqual(fromJS({}));
  });
});
