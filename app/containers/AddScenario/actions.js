/*
 *
 * AddScenario actions
 *
 */

import {
  DEFAULT_ACTION, SELECT_CHAMPION, LOAD_COMBAT_SCENARIOS, LOAD_COMBAT_SCENARIOS_SUCCESS, LOAD_COMBAT_SCENARIOS_ERROR, CHANGE_COMBAT_SCENARIO_SPECIFIC, SET_CURRENT_COMBAT_SCENARIO, REMOVE_COMBAT_SCENARIO, ADD_COMBAT_SCENARIO, POST_COMBAT_SCENARIO, POST_COMBAT_SCENARIO_ERROR, POST_COMBAT_SCENARIO_SUCCESS, CHANGE_TITLE, CHANGE_HIDDEN, COPY_SCENARIO, IMPORT_COMBAT_SCENARIO,
} from './constants';

export const defaultAction = () => ({
  type: DEFAULT_ACTION,
});


export const loadDefaultCombatScenarios = () => ({
  type: LOAD_COMBAT_SCENARIOS,
});

export const defaultCombatScenariosLoaded = (combatScenarios) => ({
  type: LOAD_COMBAT_SCENARIOS_SUCCESS,
  combatScenarios,
});

export const defaultCombatScenariosLoadingError = (error) => ({
  type: LOAD_COMBAT_SCENARIOS_ERROR,
  error,
});

export const changeCombatScenarioSpecifics = (gamePhase, combatScenarioNumber, variable, value) => ({
  type: CHANGE_COMBAT_SCENARIO_SPECIFIC,
  gamePhase,
  combatScenarioNumber,
  variable,
  value,
});

export const removeCombatScenario = (gamePhase, combatScenarioNumber) => ({
  type: REMOVE_COMBAT_SCENARIO,
  gamePhase,
  combatScenarioNumber,
});

export const addCombatScenario = (gamePhase) => ({
  type: ADD_COMBAT_SCENARIO,
  gamePhase,
});

export const setCurrentCombatScenario = (combatScenario) => ({
  type: SET_CURRENT_COMBAT_SCENARIO,
  combatScenario,
});

export const selectChampion = (championId) => ({
  type: SELECT_CHAMPION,
  championId,
});

export const changeTitle = (title) => ({
  type: CHANGE_TITLE,
  title,
});
export const changeHidden = (hidden) => ({
  type: CHANGE_HIDDEN,
  hidden,
});

export const submitCombatScenario = () => ({
  type: POST_COMBAT_SCENARIO,
});
export const submitCombatScenarioSuccess = (id) => ({
  type: POST_COMBAT_SCENARIO_SUCCESS,
  id,
});
export const submitCombatScenarioError = () => ({
  type: POST_COMBAT_SCENARIO_ERROR,
});

export const copyScenario = () => ({
  type: COPY_SCENARIO,
});
export const importScenario = (scenario) => ({
  type: IMPORT_COMBAT_SCENARIO,
  scenario,
});
