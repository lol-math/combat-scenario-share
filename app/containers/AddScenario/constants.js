/*
 *
 * AddScenario constants
 *
 */

export const DEFAULT_ACTION = 'app/AddScenario/DEFAULT_ACTION';

export const LOAD_COMBAT_SCENARIOS = 'app/AddScenario/LOAD_COMBAT_SCENARIOS';
export const LOAD_COMBAT_SCENARIOS_SUCCESS = 'app/AddScenario/LOAD_COMBAT_SCENARIOS_SUCCESS';
export const LOAD_COMBAT_SCENARIOS_ERROR = 'app/AddScenario/LOAD_COMBAT_SCENARIOS_ERROR';

export const CHANGE_COMBAT_SCENARIO_SPECIFIC = 'app/AddScenario/CHANGE_COMBAT_SCENARIO_SPECIFIC';
export const SET_CURRENT_COMBAT_SCENARIO = 'app/AddScenario/SET_CURRENT_COMBAT_SCENARIO';

export const REMOVE_COMBAT_SCENARIO = 'app/AddScenario/REMOVE_COMBAT_SCENARIO';
export const ADD_COMBAT_SCENARIO = 'app/AddScenario/ADD_COMBAT_SCENARIO';

export const POST_COMBAT_SCENARIO = 'app/AddScenario/POST_COMBAT_SCENARIO';
export const POST_COMBAT_SCENARIO_ERROR = 'app/AddScenario/POST_COMBAT_SCENARIO_ERROR';
export const POST_COMBAT_SCENARIO_SUCCESS = 'app/AddScenario/POST_COMBAT_SCENARIO_SUCCESS';

export const SELECT_CHAMPION = 'app/AddScenario/SELECT_CHAMPION';
export const CHANGE_TITLE = 'app/AddScenario/CHANGE_TITLE';
export const CHANGE_HIDDEN = 'app/AddScenario/CHANGE_HIDDEN';

export const IMPORT_COMBAT_SCENARIO = 'app/AddScenario/IMPORT_COMBAT_SCENARIO';


export const COPY_SCENARIO = 'app/AddScenario/COPY_SCENARIO';
