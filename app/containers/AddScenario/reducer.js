/*
 *
 * AddScenario reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, SELECT_CHAMPION, LOAD_COMBAT_SCENARIOS, LOAD_COMBAT_SCENARIOS_SUCCESS, LOAD_COMBAT_SCENARIOS_ERROR, CHANGE_COMBAT_SCENARIO_SPECIFIC, SET_CURRENT_COMBAT_SCENARIO, REMOVE_COMBAT_SCENARIO, ADD_COMBAT_SCENARIO, CHANGE_TITLE, CHANGE_HIDDEN, POST_COMBAT_SCENARIO, POST_COMBAT_SCENARIO_SUCCESS, POST_COMBAT_SCENARIO_ERROR,
} from './constants';

const defaultCombatScenario = {
  key: 1,
  Title: 'Scenario Name',
  Value: 100,
  Duration: 1,
  P: 0,
  Q: 0,
  W: 0,
  E: 0,
  R: 0,
  B: 0,
  C: 0,
  S: 0,
  ActivateItems: true,
  NumberOfTargets: 1,
  IgnoreCDR: false,
  IgnoreLowRangeItems: false,
  LiandryDoubleDamage: false,
};

export const initialState = fromJS({
  defaultCombatScenariosLoading: false,
  submitLoading: false,
  error: false,
  title: '',
  hidden: false,
  selectedChampion: undefined,
  defaultCombatScenarios: false,
  currentCombatScenariosSet: {
    Early: [
      { ...defaultCombatScenario },
    ],
    Mid: [
      { ...defaultCombatScenario },
    ],
    Late: [
      { ...defaultCombatScenario },
    ],
  },
});

function addScenarioReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;

    case LOAD_COMBAT_SCENARIOS:
      return state
        .set('defaultCombatScenariosLoading', true)
        .set('error', false);
    case LOAD_COMBAT_SCENARIOS_SUCCESS:
      return state
        .set('defaultCombatScenariosLoading', false)
        .set('error', false)
        .set('defaultCombatScenarios', fromJS(action.combatScenarios));
    case LOAD_COMBAT_SCENARIOS_ERROR:
      return state
        .set('defaultCombatScenariosLoading', false)
        .set('error', action.error);

    case POST_COMBAT_SCENARIO:
      return state
        .set('submitLoading', true)
        .set('error', false);
    case POST_COMBAT_SCENARIO_SUCCESS:
      return state
        .set('submitLoading', false)
        .set('error', false);
    case POST_COMBAT_SCENARIO_ERROR:
      return state
        .set('submitLoading', false)
        .set('error', action.error);

    case SET_CURRENT_COMBAT_SCENARIO:
      return state
        .set('currentCombatScenariosSet', action.combatScenario);
    case CHANGE_COMBAT_SCENARIO_SPECIFIC:
      return state
        .setIn(['currentCombatScenariosSet', action.gamePhase, action.combatScenarioNumber, action.variable], action.value);
    case REMOVE_COMBAT_SCENARIO:
      return state
        .removeIn(['currentCombatScenariosSet', action.gamePhase, action.combatScenarioNumber]);
    case ADD_COMBAT_SCENARIO:
      return state
        .updateIn(['currentCombatScenariosSet', action.gamePhase], (arr) => arr.push(fromJS(defaultCombatScenario)));
    case SELECT_CHAMPION:
      return state
        .set('selectedChampion', action.championId);
    case CHANGE_TITLE:
      return state
        .set('title', action.title);
    case CHANGE_HIDDEN:
      return state
        .set('hidden', action.hidden);
    default:
      return state;
  }
}

export default addScenarioReducer;
