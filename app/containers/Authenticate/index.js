/**
 *
 * Authenticate
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import { compose } from 'redux';

import { makeSelectUsername } from 'containers/App/selectors';
import { SHOW_IF_LOGGED_IN, SHOW_IF_NOT_LOGGED_IN } from './authenticationModes';

export default function (ComposedComponent, authMode) {
  class Authenticate extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
    redirectIfLoggedIn() {
      // TODO: if jwt token is invalid, do redirect.
      if (!this.isLoggedIn() && authMode === SHOW_IF_LOGGED_IN && !localStorage.jwtToken) {
        // this.props.addFlashMessage({
        //   type: 'error',
        //   text: 'You need to login to access this page.',
        // });
        this.props.dispatch(push('/login'));
      } else if (this.isLoggedIn() && authMode === SHOW_IF_NOT_LOGGED_IN) {
        // You are already logged in.
        this.props.dispatch(push('/'));
      }
    }
    isLoggedIn() {
      return !!this.props.username;
    }
    render() {
      this.redirectIfLoggedIn();
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }

  Authenticate.propTypes = {
    dispatch: PropTypes.func.isRequired,
    username: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.string,
    ]),
  };

  const mapStateToProps = createStructuredSelector({
    username: makeSelectUsername(),
  });

  const withConnect = connect(mapStateToProps, null);

  // return Authenticate;
  return compose(
    withConnect,
  )(Authenticate);
}

