import { createSelector } from 'reselect';

/**
 * Direct selector to the authenticate state domain
 */
const selectAuthenticateDomain = (state) => state.get('authenticate');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Authenticate
 */

const makeSelectAuthenticate = () => createSelector(
  selectAuthenticateDomain,
  (substate) => substate.toJS()
);

export default makeSelectAuthenticate;
export {
  selectAuthenticateDomain,
};
