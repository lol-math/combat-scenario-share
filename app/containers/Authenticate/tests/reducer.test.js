
import { fromJS } from 'immutable';
import authenticateReducer from '../reducer';

describe('authenticateReducer', () => {
  it('returns the initial state', () => {
    expect(authenticateReducer(undefined, {})).toEqual(fromJS({}));
  });
});
