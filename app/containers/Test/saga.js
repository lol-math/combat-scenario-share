import { take, call, put, select, all, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { DEFAULT_ACTION } from './constants';

export function* signupSuccessSaga() {
  yield put(push('/'));
}
// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield all([
    takeLatest(DEFAULT_ACTION, signupSuccessSaga),
  ]);
}
