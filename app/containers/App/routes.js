import React from 'react';

import HomePage from 'containers/HomePage/Loadable';
import Login from 'containers/Login/Loadable';
import AddScenario from 'containers/AddScenario/Loadable';
import requireAuth from 'containers/Authenticate';
import Singup from 'containers/Singup/Loadable';
import ViewCombatScenario from 'containers/ViewCombatScenario/Loadable';
import ViewUserProfile from 'containers/ViewUserProfile/Loadable';
import { SHOW_IF_LOGGED_IN, SHOW_IF_NOT_LOGGED_IN } from 'containers/Authenticate/authenticationModes';
import Test from 'containers/Test';
// import ViewCombatScenario from 'containers/ViewCombatScenario';

export default [
  {
    key: 'home',
    path: '/',
    exact: true,
    main: HomePage,
    hideFromMenuIfLoggedIn: false,
    hideFromMenuIfNotLoggedIn: false,
  },
  {
    key: 'add',
    path: '/add',
    main: requireAuth(AddScenario, SHOW_IF_LOGGED_IN),
    hideFromMenuIfLoggedIn: false,
    hideFromMenuIfNotLoggedIn: true,
  },
  {
    key: 'login',
    path: '/login',
    main: requireAuth(Login, SHOW_IF_NOT_LOGGED_IN),
    hideFromMenuIfLoggedIn: true,
    hideFromMenuIfNotLoggedIn: false,
  },
  {
    key: 'signup',
    path: '/signup',
    main: requireAuth(Singup, SHOW_IF_NOT_LOGGED_IN),
    hideFromMenuIfLoggedIn: true,
    hideFromMenuIfNotLoggedIn: false,
  },
  {
    key: 'test',
    path: '/test',
    main: Test,
    hideFromMenuIfLoggedIn: true,
    hideFromMenuIfNotLoggedIn: true,
  },
  {
    key: 'combatscenario',
    path: '/combatscenario/:id',
    main: ({ match }) => <ViewCombatScenario id={match.params.id} />,
    hideFromMenuIfLoggedIn: true,
    hideFromMenuIfNotLoggedIn: true,
  },
  {
    key: 'user',
    path: '/user/:username',
    main: ({ match }) => <ViewUserProfile username={match.params.username} />,
    hideFromMenuIfLoggedIn: true,
    hideFromMenuIfNotLoggedIn: true,
  },
];
