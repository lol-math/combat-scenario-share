import { defineMessages } from 'react-intl';

export default defineMessages({
  copyCodeButton: {
    id: 'global.copyCodeButton',
    defaultMessage: 'Copy Code',
  },
  yes: {
    id: 'global.yes',
    defaultMessage: 'Yes',
  },
  no: {
    id: 'global.no',
    defaultMessage: 'No',
  },
});
