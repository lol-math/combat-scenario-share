/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOGIN = 'app/App/LOGIN';
export const LOGIN_SUCCESS = 'app/App/LOGIN_SUCCESS';
export const LOGIN_WITH_COOKIES = 'app/App/LOGIN_WITH_COOKIES';

export const LOAD_CHAMPIONS = 'app/App/LOAD_CHAMPIONS';
export const LOAD_CHAMPIONS_SUCCESS = 'app/App/LOAD_CHAMPIONS_SUCCESS';
export const LOAD_CHAMPIONS_ERROR = 'app/App/LOAD_CHAMPIONS_ERROR';

export const LOAD_USER_DETAILS = 'app/App/LOAD_USER_DETAILS';
export const LOAD_USER_DETAILS_SUCCESS = 'app/App/LOAD_USER_DETAILS_SUCCESS';
export const LOAD_USER_DETAILS_ERROR = 'app/App/LOAD_USER_DETAILS_ERROR';

export const DELETE_COMBAT_SCENARIO = 'app/App/DELETE_COMBAT_SCENARIO';
export const DELETE_COMBAT_SCENARIO_SUCCESS = 'app/App/DELETE_COMBAT_SCENARIO_SUCCESS';
export const DELETE_COMBAT_SCENARIO_ERROR = 'app/App/DELETE_COMBAT_SCENARIO_ERROR';


export const LOGOUT = 'app/App/LOGOUT';

export const DEFAULT_LOCALE = 'en';
