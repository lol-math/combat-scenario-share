import styled from 'styled-components';

const GlobalCss = styled.div`
  h1, h2, h3, h4, h5, h6 {
    margin-bottom: 0;
  }
`;

export default GlobalCss;
