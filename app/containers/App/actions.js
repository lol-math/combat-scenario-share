/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGOUT,
  LOAD_CHAMPIONS,
  LOAD_CHAMPIONS_SUCCESS,
  LOAD_CHAMPIONS_ERROR,
  LOAD_USER_DETAILS,
  LOAD_USER_DETAILS_SUCCESS,
  LOAD_USER_DETAILS_ERROR,
  DELETE_COMBAT_SCENARIO_SUCCESS,
  DELETE_COMBAT_SCENARIO,
  DELETE_COMBAT_SCENARIO_ERROR,
} from './constants';

/**
 * Start logging in, this action starts the request saga
 *
 * @return {object} An action object with a type of LOGIN
 */
export function login() {
  return {
    type: LOGIN,
  };
}


/**
 * Dispatched when the user is logged in by the request saga
 *
 * @param  {array} jwtToken   The javascript web token
 *
 * @return {object}           An action object with a type of LOGIN_SUCCESS passing the repos
 */
export function loginSuccess(jwtToken, rememberMe) {
  return {
    type: LOGIN_SUCCESS,
    jwtToken,
    rememberMe,
  };
}


/**
 * Log out
 *
 * @return {object} An action object with a type of LOGIN
 */
export function logout() {
  return {
    type: LOGOUT,
  };
}

export const loadChampions = () => ({
  type: LOAD_CHAMPIONS,
});

export const championsLoaded = (champions) => ({
  type: LOAD_CHAMPIONS_SUCCESS,
  champions,
});

export const championsLoadingError = (error) => ({
  type: LOAD_CHAMPIONS_ERROR,
  error,
});

export const loadUserDetails = (jwtToken) => ({
  type: LOAD_USER_DETAILS,
  jwtToken,
});

export const userDetailsLoaded = (userDetails) => ({
  type: LOAD_USER_DETAILS_SUCCESS,
  userDetails,
});

export const userDetailsLoadingError = (error) => ({
  type: LOAD_USER_DETAILS_ERROR,
  error,
});

export const deleteCombatScenario = (id) => ({ type: DELETE_COMBAT_SCENARIO, id });
export const deleteCombatScenarioSuccess = (id) => ({ type: DELETE_COMBAT_SCENARIO_SUCCESS, id });
export const deleteCombatScenarioError = (id) => ({ type: DELETE_COMBAT_SCENARIO_ERROR, id });

