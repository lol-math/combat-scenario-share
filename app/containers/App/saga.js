import request from 'utils/request';
import dd from 'utils/dd';
import { fromJS } from 'immutable';
import { takeOneAndBlock } from 'utils/customSagaEffects';
import { call, put, takeLatest, all, select } from 'redux-saga/effects';
import { championsLoaded, championsLoadingError, userDetailsLoadingError, userDetailsLoaded, deleteCombatScenarioSuccess, deleteCombatScenarioError } from './actions';
import { LOAD_CHAMPIONS, LOGIN_SUCCESS, DELETE_COMBAT_SCENARIO, LOGOUT } from './constants';
import { loginError } from '../Login/actions';
import { url } from 'utils/server';
import { makeSelectJwtToken } from './selectors';


/**
 * Champions request/response handler
 */
export function* getChampions() {
  try {
    yield call(getLatestPatch);
    const champions = yield call(request, dd.data.championsFull());
    const championData = {};
    Object.values(champions.data).forEach((element) => {
      championData[element.id] = {
        name: element.name,
        spells: element.spells,
        passive: element.passive,
        key: element.key,
        id: element.id,
        image: element.image,
      };
    });
    yield put(championsLoaded(fromJS(championData)));
  } catch (err) {
    yield put(championsLoadingError(err));
  }
}


export function* getLatestPatch() {
  try {
    const ddVersions = yield call(request, dd.versions());
    dd.version = ddVersions[0];
    return ddVersions[0];
  } catch (err) {
    return err;
  }
}

/**
 * Side effects on login success
 */
export function* onLoginSuccessSaga(action) {
  if (action.rememberMe) {
    localStorage.setItem('jwtToken', action.jwtToken);
  }
}
/**
 * Side effects on login success
 */
export function* removeJwtTokenSaga() {
  localStorage.removeItem('jwtToken');
}

/**
 * Get User Details for JWT token
 */
export function* loadUserDetailsSaga(action) {
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${action.jwtToken}`,
      },
      body: JSON.stringify({
        query: `
          {
            currentPerson {
              name,
              id
            }
          }
        `,
      }),
    });
    yield put(userDetailsLoaded(fromJS(response.data.currentPerson)));
  } catch (err) {
    yield put(userDetailsLoadingError(err));
  }
}

/**
 * delete a combatScenario using an id
 */
export function* deleteCombatScenarioSaga(action) {
  const jwtToken = yield select(makeSelectJwtToken());
  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${jwtToken}`,
      },
      body: JSON.stringify({
        query: `
          mutation {
            deleteCombatscenarioById(input: {id: ${action.id}}) {
              deletedCombatscenarioId
            }
          }
        `,
      }),
    });
    yield put(deleteCombatScenarioSuccess(fromJS(response.data.currentPerson)));
  } catch (err) {
    yield put(deleteCombatScenarioError(err));
  }
}


/**
 * Root saga manages watcher lifecycle
 */
export default function* globalSaga() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(LOAD_CHAMPIONS, getChampions);
  yield all([
    takeLatest(LOAD_CHAMPIONS, getChampions),
    takeLatest(LOGIN_SUCCESS, onLoginSuccessSaga),
    takeLatest(LOGIN_SUCCESS, loadUserDetailsSaga),
    takeLatest(LOGOUT, removeJwtTokenSaga),
    takeLatest(DELETE_COMBAT_SCENARIO, deleteCombatScenarioSaga),
  ]);
}
