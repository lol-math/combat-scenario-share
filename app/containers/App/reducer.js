/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  LOGIN_SUCCESS,
  LOGOUT,
  LOAD_CHAMPIONS,
  LOAD_CHAMPIONS_SUCCESS,
  LOAD_CHAMPIONS_ERROR,
  LOAD_USER_DETAILS_ERROR,
  LOAD_USER_DETAILS_SUCCESS,
} from './constants';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  error: false,
  championInfo: { empty: true },
  user: false,
  jwtToken: false,
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_CHAMPIONS:
      return state
        .set('loading', true)
        .set('error', false);
    case LOAD_CHAMPIONS_SUCCESS:
      return state
        .set('loading', false)
        .set('error', false)
        .set('championInfo', fromJS(action.champions));
    case LOAD_CHAMPIONS_ERROR:
      return state
        .set('loading', false)
        .set('error', action.error);
    case LOGIN_SUCCESS:
      return state
        .set('jwtToken', action.jwtToken);
    case LOAD_USER_DETAILS_SUCCESS:
      return state
        .set('user', action.userDetails);
    case LOAD_USER_DETAILS_ERROR:
      return state
        .set('userdetailsLoadingError', action.error);
    case LOGOUT:
      return state
        .set('user', false)
        .set('jwtToken', false);
    default:
      return state;
  }
}

export default appReducer;
