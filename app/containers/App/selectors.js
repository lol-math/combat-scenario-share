/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const selectRoute = (state) => state.get('route');

const makeSelectUser = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('user')
);

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);


const makeSelectUsername = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['user', 'name'])
);

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

const makeSelectChampionList = () => createSelector(
  selectGlobal,
  (globalState) => Object.values(globalState.get('championInfo').toJS()).sort((a, b) => a.name > b.name)
);


const makeSelectChampionInfo = (championId) => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['championInfo', championId])
);

const makeSelectChampionInfoSpecific = (championId, variable) => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['championInfo', championId, variable], {})
);

const makeSelectChampionsInfo = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('championInfo')
);

export const makeSelectJwtToken = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('jwtToken')
);

export {
  selectGlobal,
  makeSelectUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectLocation,
  makeSelectUsername,
  makeSelectChampionInfo,
  makeSelectChampionInfoSpecific,
  makeSelectChampionsInfo,
  makeSelectChampionList,
};
