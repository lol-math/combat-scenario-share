/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';
import NavBar from 'containers/NavBar';


import BaseContent from '../../components/BaseContent';
import routes from './routes';
import { withRouter } from 'react-router-dom';


import { connect } from 'react-redux';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import saga from './saga';
import { loginSuccess } from './actions';
import { makeSelectLocation } from './selectors';
import { createStructuredSelector } from 'reselect';
import Proptypes from 'prop-types';
import GlobalCss from './GlobalCss';


const AppLayout = styled(Layout)`
  display: flex;
  flex: 1;
  min-height: 100vh;
`;

const ContentWrapper = styled.div`
  background: white;
  padding: 24px;
  min-height: 280;
`;

class App extends React.Component {
  componentWillMount() {
    if (localStorage.jwtToken) {
      this.props.loginWithToken(localStorage.jwtToken);
    }
  }

  render() {
    return (
      <AppLayout>
        <Helmet
          titleTemplate="%s - Combat Scenarios"
          defaultTitle="Item Optimizer"
        >
          <meta name="description" content="OP Items Combat Scenarios" />
        </Helmet>
        <NavBar
          routes={routes}
        />
        <BaseContent>
          <ContentWrapper>
            <GlobalCss>
              <Switch location={this.props.location}>
                {routes.map((route) => <Route key={route.path} path={route.path} component={route.main} exact={route.exact} />)}
              </Switch>
            </GlobalCss>
          </ContentWrapper>
        </BaseContent>
      </AppLayout>
    );
  }
}


App.propTypes = {
  loginWithToken: Proptypes.func.isRequired,
};
const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
});

function mapDispatchToProps(dispatch) {
  return {
    loginWithToken: (token) => dispatch(loginSuccess(token, true)),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'app', saga });

export default compose(
  withSaga,
  withConnect,
)(App);
