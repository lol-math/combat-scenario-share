
import { fromJS } from 'immutable';
import singupReducer from '../reducer';

describe('singupReducer', () => {
  it('returns the initial state', () => {
    expect(singupReducer(undefined, {})).toEqual(fromJS({}));
  });
});
