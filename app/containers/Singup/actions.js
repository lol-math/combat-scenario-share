/*
 *
 * Singup actions
 *
 */

import {
  DEFAULT_ACTION, SIGNUP, SIGNUP_SUCCESS, SIGNUP_ERROR, CHANGE_USERNAME, CHANGE_PASSWORD, CHANGE_EMAIL,
} from './constants';

/**
 * Start sign up, this action starts the request saga
 *
 * @return {object} An action object with a type of LOGIN
 */
export function signup() {
  return {
    type: SIGNUP,
  };
}

/**
 * Dispatched when the user has successfully signed up.
 *
 * @return {object} An action object with a type of SIGNUP_SUCCESS
 */
export function signupSuccess() {
  return {
    type: SIGNUP_SUCCESS,
  };
}

/**
 * Dispatched when signing up fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of SIGNUP_ERROR passing the error
 */
export function signupError(error) {
  return {
    type: SIGNUP_ERROR,
    error,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} username The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {password} password The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_PASSWORD
 */
export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {password} email The new text of the input field
 *
 * @return {object}         An action object with a type of CHANGE_EMAIL
 */
export function changeEmail(email) {
  return {
    type: CHANGE_EMAIL,
    email,
  };
}

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
