import jwt from 'jsonwebtoken';
import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import request from 'utils/request';
// import { push } from 'react-router-redux';
import { push } from 'react-router-dom';
import { makeSelectUsername, makeSelectPassword, makeSelectEmail } from './selectors';
import { SIGNUP, SIGNUP_SUCCESS } from './constants';
import { signupSuccess, signupError } from './actions';
import { url } from 'utils/server';

export function* postSignup() {
  // Select username from store
  const authDetails = {
    name: yield select(makeSelectUsername()),
    password: yield select(makeSelectPassword()),
    email: yield select(makeSelectEmail()),
  };

  try {
    yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `
          mutation RegisterPerson($input: RegisterPersonInput! ){
            registerPerson(input: $input) {
              person{
                id
                name
              }
            }
          }`,
        variables: {
          input: {
            ...authDetails,
          },
        },
      }),
    });
    yield put(signupSuccess(authDetails));
  } catch (err) {
    yield put(signupError(err));
  }
}
export function* signupSuccessSaga() {
  yield put(push('/login'));
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield all([
    takeLatest(SIGNUP_SUCCESS, signupSuccessSaga),
    takeLatest(SIGNUP, postSignup),
  ]);
}
