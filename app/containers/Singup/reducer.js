/*
 *
 * Singup reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION, CHANGE_PASSWORD, CHANGE_USERNAME, CHANGE_EMAIL, SIGNUP_SUCCESS, SIGNUP, SIGNUP_ERROR,
} from './constants';

export const initialState = fromJS({
  username: '',
  password: '',
  email: '',
});

function singupReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USERNAME:
      return state
        .set('username', action.username);
    case CHANGE_PASSWORD:
      return state
        .set('password', action.password);
    case CHANGE_EMAIL:
      return state
        .set('email', action.email);
    case SIGNUP:
      return state
        .set('error', false);
    case SIGNUP_ERROR:
      return state
        .set('error', action.error);
    case SIGNUP_SUCCESS:
      return state
        .set('username', '')
        .set('password', '')
        .set('email', '');
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default singupReducer;
