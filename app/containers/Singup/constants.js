/*
 *
 * Singup constants
 *
 */

export const DEFAULT_ACTION = 'app/Singup/DEFAULT_ACTION';

export const CHANGE_USERNAME = 'app/Singup/CHANGE_USERNAME';
export const CHANGE_PASSWORD = 'app/Singup/CHANGE_PASSWORD';
export const CHANGE_EMAIL = 'app/Singup/CHANGE_EMAIL';

export const SIGNUP = 'app/Singup/SIGNUP';
export const SIGNUP_SUCCESS = 'app/Singup/SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'app/Singup/SIGNUP_ERROR';
