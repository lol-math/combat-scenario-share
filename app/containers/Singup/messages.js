/*
 * Singup Messages
 *
 * This contains all the text for the Singup component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  signup: {
    id: 'app.containers.Signup.signup',
    defaultMessage: 'Sign Up',
  },
  placeholderPassword: {
    id: 'app.containers.Signup.placeholderPassword',
    defaultMessage: 'Password',
  },
  placeholderUserName: {
    id: 'app.containers.Signup.placeholderUserName',
    defaultMessage: 'Username',
  },
  passwordNotValidMessage: {
    id: 'app.containers.Signup.passwordNotValidMessage',
    defaultMessage: 'Your password must be at least 6 characters.',
  },
  usernameNotValidMessage: {
    id: 'app.containers.Signup.usernameNotValidMessage',
    defaultMessage: 'Your username must not be empty.',
  },
  emailNotValidMessage: {
    id: 'app.containers.Signup.usernameNotValidMessage',
    defaultMessage: 'You must enter a valid email address.',
  },
  placeholderEmail: {
    id: 'app.containers.Signup.placeholderEmail',
    defaultMessage: 'example@lolmath.net',
  },
});
