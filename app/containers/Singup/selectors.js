import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the singup state domain
 */
const selectSingupDomain = (state) => state.get('singup', initialState);

/**
 * Other specific selectors
 */
const makeSelectUsername = () => createSelector(
  selectSingupDomain,
  (signupState) => signupState.get('username')
);
const makeSelectPassword = () => createSelector(
  selectSingupDomain,
  (signupState) => signupState.get('password')
);
const makeSelectEmail = () => createSelector(
  selectSingupDomain,
  (signupState) => signupState.get('email')
);
/**
 * Default selector used by Singup
 */

const makeSelectSingup = () => createSelector(
  selectSingupDomain,
  (substate) => substate.toJS()
);

export default makeSelectSingup;
export {
  selectSingupDomain,
  makeSelectUsername,
  makeSelectPassword,
  makeSelectEmail,
};
