/**
 *
 * Singup
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import emailValidator from 'email-validator';

import { isValidUsername, isValidPassword } from 'utils/validators';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSingup, { makeSelectEmail } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { makeSelectPassword, makeSelectUsername } from './selectors';
import { changeUsername, changePassword, signup, changeEmail } from './actions';

const FormItem = Form.Item;

const FormSmall = styled(Form)`
  max-width: 300px;
  margin: 0 auto;
`;
const ButtonFullWidth = styled(Button)`
  width: 100%;
`;

class Singup extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet>
          <title>Sing up</title>
          <meta name="description" content="Sign up to Combat Scenario Share" />
        </Helmet>
        <FormSmall
          onSubmit={this.props.onSubmitForm}
        >
          <FormItem
            validateStatus={isValidUsername(this.props.username) ? '' : 'error'}
            hasFeedback
            help={isValidUsername(this.props.username) ? null : <FormattedMessage {...messages.usernameNotValidMessage} />}
          >
            <FormattedMessage {...messages.placeholderUserName}>
              {(placeholderUserName) => (
                <Input
                  prefix={<Icon type="user" />}
                  style={{ color: 'rgba(0,0,0,.25)' }}
                  value={this.props.username}
                  onChange={this.props.onChangeUsername}
                  placeholder={placeholderUserName}
                />
              )}
            </FormattedMessage >
          </FormItem>
          <FormItem
            validateStatus={emailValidator.validate(this.props.email) ? '' : 'error'}
            hasFeedback
            help={emailValidator.validate(this.props.email) ? null : <FormattedMessage {...messages.emailNotValidMessage} />}
          >
            <FormattedMessage {...messages.placeholderEmail}>
              {(placeholderUserName) => (
                <Input
                  prefix={<Icon type="mail" />}
                  style={{ color: 'rgba(0,0,0,.25)' }}
                  value={this.props.email}
                  onChange={this.props.onChangeEmail}
                  placeholder={placeholderUserName}
                />
              )}
            </FormattedMessage >
          </FormItem>
          <FormItem
            validateStatus={isValidPassword(this.props.password) ? '' : 'error'}
            hasFeedback
            help={isValidPassword(this.props.password) ? null : <FormattedMessage {...messages.passwordNotValidMessage} />}
          >

            {/* <FormattedMessage {...messages.placeholderPassword}>
              {(placeholderPassword) => (
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  value={this.props.password}
                  onChange={this.props.onChangePassword}
                  placeholder={placeholderPassword}
                />

              )}
            </FormattedMessage> */}
            <FormattedMessage {...messages.placeholderPassword}>
              {(placeholderPassword) => (
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  value={this.props.password}
                  onChange={this.props.onChangePassword}
                  placeholder={placeholderPassword}
                />

              )}
            </FormattedMessage>
          </FormItem>
          <FormItem>
            <ButtonFullWidth
              type="primary"
              htmlType="submit"
              disabled={!(emailValidator.validate(this.props.email) && isValidUsername(this.props.username) && isValidPassword(this.props.password))}
            >
              <FormattedMessage {...messages.signup} />
            </ButtonFullWidth>
          </FormItem>
        </FormSmall>
      </div>
    );
  }
}

Singup.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  singup: makeSelectSingup(),
  username: makeSelectUsername(),
  password: makeSelectPassword(),
  email: makeSelectEmail(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onChangePassword: (evt) => dispatch(changePassword(evt.target.value)),
    onChangeEmail: (evt) => dispatch(changeEmail(evt.target.value)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(signup());
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'singup', reducer });
const withSaga = injectSaga({ key: 'singup', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Singup);
