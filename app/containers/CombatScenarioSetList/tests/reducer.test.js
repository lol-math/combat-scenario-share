
import { fromJS } from 'immutable';
import combatScenarioSetListReducer from '../reducer';

describe('combatScenarioSetListReducer', () => {
  it('returns the initial state', () => {
    expect(combatScenarioSetListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
