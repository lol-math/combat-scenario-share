/**
 *
 * CombatScenarioSetList
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import CombatScenarioSetListItem from 'components/CombatScenarioSetListItem';
import { List, Icon } from 'antd';
import { makeSelectChampionsInfo } from 'containers/App/selectors';
import { loadChampions } from '../App/actions';
import reducer from './reducer';
import saga from './saga';
import moment from 'moment/moment.js';
import messages from './messages';
import { makeSelectUsername } from 'containers/App/selectors';
import { deleteCombatScenario } from 'containers/App/actions';
require('moment/locale/nl');

class CombatScenarioSetList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.onInitialLoad();
  }
  render() {
    moment().locale('nl');
    return (
      <List
        pagination={{
          total: this.props.combatScenarioSets.get('totalCount'),
          pageSize: 10,
          onChange: this.props.onChangePage,
          hideOnSinglePage: true,
        }}
        loading={{ spinning: this.props.loading, indicator: <Icon type="loading" style={{ fontSize: 24 }} spin /> }}
        dataSource={this.props.combatScenarioSets.get('nodes')}
        renderItem={(item) => (<CombatScenarioSetListItem
          key={item.get('id')}
          championImage={this.props.championsInfo.getIn([item.get('championId'), 'image', 'full'], false)}
          championName={this.props.championsInfo.getIn([item.get('championId'), 'name'], 'loading...')}
          title={item.get('title')}
          id={item.get('id')}
          published={item.get('published')}
          author={item.getIn(['personByAuthorId', 'name'])}
          currentUserName={this.props.userName}
          dateCreated={moment(item.get('createdAt')).parseZone().local().format('LLL')}
          onDeleteCombatScenario={this.props.onDeleteCombatScenario}
        />)}
      />
    );
  }
}

CombatScenarioSetList.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onInitialLoad: PropTypes.func.isRequired,
  combatScenarioSets: PropTypes.object.isRequired,
  championsInfo: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  championsInfo: makeSelectChampionsInfo(),
  userName: makeSelectUsername(),
});

function mapDispatchToProps(dispatch) {
  return {
    onInitialLoad: () => dispatch(loadChampions()),
    onDeleteCombatScenario: (id) => dispatch(deleteCombatScenario(id)),
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'combatScenarioSetList', reducer });
const withSaga = injectSaga({ key: 'combatScenarioSetList', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CombatScenarioSetList);
