/**
 *
 * Asynchronously loads the component for CombatScenarioSetList
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
