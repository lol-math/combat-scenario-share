import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the combatScenarioSetList state domain
 */
const selectCombatScenarioSetListDomain = (state) => state.get('combatScenarioSetList', initialState);

/**
 * Other specific selectors
 */


/**
 * Default selector used by CombatScenarioSetList
 */

const makeSelectCombatScenarioSetList = () => createSelector(
  selectCombatScenarioSetListDomain,
  (substate) => substate.toJS()
);

export default makeSelectCombatScenarioSetList;
export {
  selectCombatScenarioSetListDomain,
};
