import { all } from 'redux-saga/effects';
import { takeOneAndBlock } from 'utils/customSagaEffects';

import { getChampions } from 'containers/App/saga';
import { LOAD_CHAMPIONS } from 'containers/App/constants';

// Individual exports for testing
export default function* defaultSaga() {
  yield all([
    yield takeOneAndBlock(LOAD_CHAMPIONS, getChampions),
  ]);
}
