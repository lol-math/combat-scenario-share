/*
 * Login Messages
 *
 * This contains all the text for the Login component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  login: {
    id: 'app.containers.Login.login',
    defaultMessage: 'Log in',
  },
  registerNow: {
    id: 'app.containers.Login.registerNow',
    defaultMessage: 'Register Now',
  },
  or: {
    id: 'app.containers.Login.or',
    defaultMessage: 'Or',
  },
  forgotPassword: {
    id: 'app.containers.Login.forgotPassword',
    defaultMessage: 'Forgot password',
  },
  rememberMe: {
    id: 'app.containers.Login.rememberMe',
    defaultMessage: 'Remember me',
  },
  placeholderPassword: {
    id: 'app.containers.Login.placeholderPassword',
    defaultMessage: 'Password',
  },
  placeholderUserName: {
    id: 'app.containers.Login.placeholderUserName',
    defaultMessage: 'Email',
  },
  passwordNotInputtedMessage: {
    id: 'app.containers.Login.passwordNotInputtedMessage',
    defaultMessage: 'Please input your password!',
  },
  usernameNotInputtedMessage: {
    id: 'app.containers.Login.usernameNotInputtedMessage',
    defaultMessage: 'Please input your username!',
  },
});
