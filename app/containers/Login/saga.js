import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import { LOGIN } from 'containers/App/constants';
import { loginSuccess } from 'containers/App/actions';

import { url } from 'utils/server';
import request from 'utils/request';
import { makeSelectUsername, makeSelectPassword, makeSelectRememberMe } from './selectors';
import { loginError } from './actions';

/**
 * Login request/response handler
 */
export function* postLogin() {
  // Select username from store
  const authDetails = {
    name: yield select(makeSelectUsername()),
    password: yield select(makeSelectPassword()),
    rememberMe: yield select(makeSelectRememberMe()),
  };

  try {
    const response = yield call(request, url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: `
          mutation{
            authenticate(input: {
              email: "${authDetails.name}",
              password: "${authDetails.password}",
            }) {
              jwtToken
            }
          }
          `,
      }),
    });
    yield put(loginSuccess(response.data.authenticate.jwtToken, authDetails.rememberMe));
  } catch (err) {
    console.log(err);
    yield put(loginError(err.message));
  }
}

// Individual exports for testing
export default function* defaultSaga() {
  // See example in containers/HomePage/saga.js
  yield all([
    takeLatest(LOGIN, postLogin),
  ]);
}
