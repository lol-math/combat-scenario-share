/*
 *
 * Login reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_USERNAME,
  CHANGE_PASSWORD,
  LOGIN_ERROR,
  CHANGE_REMEMBER_ME,
} from './constants';
import { LOGIN, LOGIN_SUCCESS } from '../App/constants';

export const initialState = fromJS({
  username: '',
  password: '',
  rememberMe: false,
  loading: false,
  error: false,
});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_USERNAME:
      return state
        .set('username', action.username);
    case CHANGE_PASSWORD:
      return state
        .set('password', action.password);
    case CHANGE_REMEMBER_ME:
      return state
        .set('rememberMe', action.rememberMe);
    case LOGIN:
      return state
        .set('loading', true)
        .set('error', false);
    case LOGIN_SUCCESS:
      return state
        .set('loading', false);
    case LOGIN_ERROR:
      return state
        .set('error', action.error)
        .set('loading', false);
    default:
      return state;
  }
}

export default loginReducer;
