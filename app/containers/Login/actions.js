/*
 *
 * Login actions
 *
 */

import {
  CHANGE_USERNAME,
  CHANGE_PASSWORD,
  LOGIN_ERROR,
  CHANGE_REMEMBER_ME,
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {name} username The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(username) {
  return {
    type: CHANGE_USERNAME,
    username,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {password} password The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_PASSWORD
 */
export function changePassword(password) {
  return {
    type: CHANGE_PASSWORD,
    password,
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {password} password The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_PASSWORD
 */
export function changeRememberMe(rememberMe) {
  return {
    type: CHANGE_REMEMBER_ME,
    rememberMe,
  };
}

/**
 * Dispatched when logging in fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOGIN_ERROR passing the error
 */
export function loginError(error) {
  return {
    type: LOGIN_ERROR,
    error,
  };
}
