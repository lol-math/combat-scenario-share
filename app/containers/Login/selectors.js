import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the login state domain
 */
const selectLoginDomain = (state) => state.get('login', initialState);

/**
 * Other specific selectors
 */


/**
 * Default selector used by Login
 */

const makeSelectLogin = () => createSelector(
  selectLoginDomain,
  (substate) => substate.toJS()
);
const makeSelectUsername = () => createSelector(
  selectLoginDomain,
  (loginState) => loginState.get('username')
);
const makeSelectPassword = () => createSelector(
  selectLoginDomain,
  (loginState) => loginState.get('password')
);
const makeSelectRememberMe = () => createSelector(
  selectLoginDomain,
  (loginState) => loginState.get('rememberMe')
);
const makeSelectLoading = () => createSelector(
  selectLoginDomain,
  (loginState) => loginState.get('loading')
);
const makeSelectError = () => createSelector(
  selectLoginDomain,
  (loginState) => loginState.get('error')
);


export default makeSelectLogin;
export {
  selectLoginDomain,
  makeSelectUsername,
  makeSelectPassword,
  makeSelectLoading,
  makeSelectError,
  makeSelectRememberMe,
};
