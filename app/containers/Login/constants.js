/*
 *
 * Login constants
 *
 */

export const CHANGE_USERNAME = 'app/Login/CHANGE_USERNAME';
export const CHANGE_PASSWORD = 'app/Login/CHANGE_PASSWORD';
export const LOGIN_ERROR = 'item-optimizer-combat-scenario-share/Login/LOGIN_ERROR';
export const CHANGE_REMEMBER_ME = 'item-optimizer-combat-scenario-share/Login/CHANGE_REMEMBER_ME';
