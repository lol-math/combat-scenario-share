/**
 *
 * Login
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Form, Icon, Input, Button, Checkbox, Alert } from 'antd';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { isValidUsername, isValidPassword } from 'utils/validators';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { login } from 'containers/App/actions';
import makeSelectLogin, { makeSelectPassword, makeSelectUsername, makeSelectLoading, makeSelectError, makeSelectRememberMe } from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { changeUsername, changePassword, changeRememberMe } from './actions';

const FormItem = Form.Item;

const FormSmall = styled(Form)`
  max-width: 300px;
  margin: 0 auto;
`;
const RightAlignedA = styled.a`
  float: right;
`;
const ButtonFullWidth = styled(Button)`
  width: 100%;
`;

export class Login extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet>
          <title>Log in</title>
          <meta name="description" content="Log into your account" />
        </Helmet>
        <FormSmall
          onSubmit={this.props.onSubmitForm}
        >
          <h2><FormattedMessage {...messages.login} /></h2>
          {this.props.error && <Alert type={'error'} message={this.props.error} />}
          <FormItem >
            <FormattedMessage {...messages.placeholderUserName}>
              {(placeholderUserName) => (
                <Input
                  prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  value={this.props.username}
                  onChange={this.props.onChangeUsername}
                  placeholder={placeholderUserName}
                />
              )}
            </FormattedMessage >
          </FormItem>
          <FormItem >

            <FormattedMessage {...messages.placeholderPassword}>
              {(placeholderPassword) => (
                <Input
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="password"
                  value={this.props.password}
                  onChange={this.props.onChangePassword}
                  placeholder={placeholderPassword}
                />
              )}
            </FormattedMessage>

          </FormItem>
          <FormItem>

            <Checkbox checked={this.props.rememberMe} onChange={this.props.onChangeRememberMe}><FormattedMessage {...messages.rememberMe} /> </Checkbox>

            <RightAlignedA href=""> <FormattedMessage {...messages.forgotPassword} /></RightAlignedA>
            <ButtonFullWidth
              type="primary"
              htmlType="submit"
              loading={this.props.loading}
              // disabled={!(isValidUsername(this.props.username) && isValidPassword(this.props.password))}

            >
              <FormattedMessage {...messages.login} />
            </ButtonFullWidth>
            <FormattedMessage {...messages.or} /> <Link to="/signup"> <FormattedMessage {...messages.registerNow} /></Link>
          </FormItem>
        </FormSmall>
      </div>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  onSubmitForm: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  rememberMe: PropTypes.bool.isRequired,
  onChangePassword: PropTypes.func.isRequired,
  onChangeUsername: PropTypes.func.isRequired,
  onChangeRememberMe: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool.isRequired,
};

const mapStateToProps = createStructuredSelector({
  login: makeSelectLogin(),
  username: makeSelectUsername(),
  password: makeSelectPassword(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  rememberMe: makeSelectRememberMe(),
});

function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: (evt) => dispatch(changeUsername(evt.target.value)),
    onChangePassword: (evt) => dispatch(changePassword(evt.target.value)),
    onChangeRememberMe: (evt) => dispatch(changeRememberMe(evt.target.checked)),
    onSubmitForm: (evt) => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(login());
    },
    dispatch,
  };
}


const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Login);
