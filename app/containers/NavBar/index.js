/**
 *
 * NavBar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { makeSelectUsername } from 'containers/App/selectors';
import injectReducer from 'utils/injectReducer';
import Navigation from 'components/Navigation';
import { withRouter } from 'react-router-dom';
import { logout } from 'containers/App/actions';
import reducer from './reducer';
import { makeSelectLocation } from '../App/selectors';

class NavBar extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  isLoggedIn() {
    return !!this.props.username;
  }

  render() {
    return (
      <Navigation
        location={this.props.location}
        routes={this.props.routes.filter((route) => {
          if (route.hideFromMenuIfLoggedIn && this.isLoggedIn()) return false;
          if (route.hideFromMenuIfNotLoggedIn && !this.isLoggedIn()) return false;
          return true;
        })}
        username={this.props.username}
        logout={this.props.onLogout}
      />
    );
  }
}
NavBar.propTypes = {
  dispatch: PropTypes.func.isRequired,
  routes: PropTypes.arrayOf(PropTypes.shape({
    path: PropTypes.string,
    key: PropTypes.string,
    exact: PropTypes.bool,
    main: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string,
      PropTypes.node,
    ]),
    hideFromMenuIfLoggedIn: PropTypes.bool,
    hideFromMenuIfNotLoggedIn: PropTypes.bool,
  })),
  username: PropTypes.string,
  onLogout: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
  username: makeSelectUsername(),
});

function mapDispatchToProps(dispatch) {
  return {
    onLogout: () => dispatch(logout()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'navBar', reducer });

export default compose(
  withRouter,
  withConnect,
)(NavBar);
