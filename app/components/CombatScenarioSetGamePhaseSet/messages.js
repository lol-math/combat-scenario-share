/*
 * CombatScenarioSetGamePhaseSet Messages
 *
 * This contains all the text for the CombatScenarioSetGamePhaseSet component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.CombatScenarioSetGamePhaseSet.header',
    defaultMessage: 'This is the CombatScenarioSetGamePhaseSet component !',
  },
});
