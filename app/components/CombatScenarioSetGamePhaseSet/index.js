/**
*
* CombatScenarioSetGamePhaseSet
*
*/

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import CombatScenarioSet from 'components/CombatScenarioSet';
import GamePhaseHeader from './GamePhaseHeader';

function CombatScenarioSetGamePhaseSet({ Early, Mid, Late }) {
  return (
    <div>
      <GamePhaseHeader>Early</GamePhaseHeader>
      <CombatScenarioSet combatScenarioSet={Early} gamePhase="early" />
      <GamePhaseHeader>Mid</GamePhaseHeader>
      <CombatScenarioSet combatScenarioSet={Mid} gamePhase="mid" />
      <GamePhaseHeader>Late</GamePhaseHeader>
      <CombatScenarioSet combatScenarioSet={Late} gamePhase="late" />
    </div>
  );
}

CombatScenarioSetGamePhaseSet.propTypes = {

};

export default CombatScenarioSetGamePhaseSet;
