/**
*
* CombatScenario
*
*/

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import PropTypes from 'prop-types';
import React from 'react';
import CombatScenarioWrapper from './CombatScenarioWrapper';
import Stat from './Stat';
import TitleElement from './TitleElement';
import StatWrapper from './StatWrapper';
import BoolValue from './BoolValue';

const CombatScenario = ({
  B,
  NumberOfTargets,
  C,
  E,
  IgnoreLowRangeItems,
  ActivateItems,
  P,
  Q,
  Value,
  R,
  S,
  Duration,
  W,
  Title,
  IgnoreCDR,
  LiandryDoubleDamage,
}) =>
  (
    <CombatScenarioWrapper>
      <TitleElement>{Title} ({Value}%)</TitleElement>
      <StatWrapper>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.activateItems} /></span><BoolValue value={ActivateItems} /></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.burstAttack} /></span><BoolValue value={IgnoreCDR} /></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.rangedAttack} /></span><BoolValue value={IgnoreLowRangeItems} /></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.liandryDoubleDamage} /></span><BoolValue value={LiandryDoubleDamage} /></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.numberOfTargets} /></span><span className="StatValue">{NumberOfTargets}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.combatDuration} /></span><span className="StatValue">{Duration}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.basicAttacks} /></span><span className="StatValue">{B}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.basicAttackSeconds} /></span><span className="StatValue">{C}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.skillPassive} /></span><span className="StatValue">{P}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.skillQ} /></span><span className="StatValue">{Q}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.skillW} /></span><span className="StatValue">{W}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.skillE} /></span><span className="StatValue">{E}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.skillR} /></span><span className="StatValue">{R}</span></Stat>
        <Stat><span className="StatTitle"><FormattedMessage {...messages.sheenProcs} /></span><span className="StatValue">{S}</span></Stat>
      </StatWrapper>
    </CombatScenarioWrapper>
  );

CombatScenario.propTypes = {
  B: PropTypes.number.isRequired,
  NumberOfTargets: PropTypes.number.isRequired,
  C: PropTypes.number.isRequired,
  E: PropTypes.number.isRequired,
  IgnoreLowRangeItems: PropTypes.any.isRequired,
  ActivateItems: PropTypes.bool.isRequired,
  P: PropTypes.number.isRequired,
  Q: PropTypes.number.isRequired,
  Value: PropTypes.number.isRequired,
  R: PropTypes.number.isRequired,
  S: PropTypes.number.isRequired,
  Duration: PropTypes.number.isRequired,
  W: PropTypes.number.isRequired,
  Title: PropTypes.string.isRequired,
  IgnoreCDR: PropTypes.any.isRequired,
  LiandryDoubleDamage: PropTypes.bool.isRequired,
};

export default CombatScenario;
