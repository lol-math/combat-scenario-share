import styled from 'styled-components';

const TitleElement = styled.h3`
  margin-bottom: 0;
`;

export default TitleElement;
