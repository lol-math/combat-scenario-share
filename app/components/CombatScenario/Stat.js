import styled from 'styled-components';

const StatSpan = styled.div`
  &:nth-child(even){
    background-color: #eee;
  }
  .StatTitle {
    display: inline-block;
  }
  .StatValue{
    display: inline-block;
    float: right;
  }
`;

export default StatSpan;
