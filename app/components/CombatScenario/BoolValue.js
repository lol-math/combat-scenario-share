import React from 'react';

export default ({ value }) => (
  <div className="StatValue">
    {value ? '✔️' : '❌'} ️
  </div>
  );
