/*
 * CombatScenario Messages
 *
 * This contains all the text for the CombatScenario component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  basicAttacks: {
    id: 'app.components.CombatScenario.basicAttack',
    defaultMessage: 'Basic attacks (flat)',
  },
  numberOfTargets: {
    id: 'app.components.CombatScenario.numberOfTargets',
    defaultMessage: 'Number of targets',
  },
  title: {
    id: 'app.components.CombatScenario.title',
    defaultMessage: 'Title',
  },
  basicAttackSeconds: {
    id: 'app.components.CombatScenario.basicAttackSeconds',
    defaultMessage: 'Basic attacks (seconds)',
  },
  rangedAttack: {
    id: 'app.components.CombatScenario.ignoreLowRangeItems',
    defaultMessage: 'Range Attack',
  },
  activateItems: {
    id: 'app.components.CombatScenario.activateItems',
    defaultMessage: 'Activate Items',
  },
  skillPassive: {
    id: 'app.components.CombatScenario.passive',
    defaultMessage: 'Passive',
  },
  skillQ: {
    id: 'app.components.CombatScenario.skillQ',
    defaultMessage: 'Q',
  },
  skillW: {
    id: 'app.components.CombatScenario.skillW',
    defaultMessage: 'W',
  },
  skillE: {
    id: 'app.components.CombatScenario.skillE',
    defaultMessage: 'E',
  },
  skillR: {
    id: 'app.components.CombatScenario.skillR',
    defaultMessage: 'R',
  },
  sheenProcs: {
    id: 'app.components.CombatScenario.sheenProcs',
    defaultMessage: 'Sheen procs',
  },
  liandryDoubleDamage: {
    id: 'app.components.CombatScenario.liandryDoubleDamage',
    defaultMessage: 'Liandry\'s deals Double Damage',
  },
  burstAttack: {
    id: 'app.components.CombatScenario.burstAttack',
    defaultMessage: 'Burst Attack',
  },
  combatDuration: {
    id: 'app.components.CombatScenario.combatDuration',
    defaultMessage: 'Combat duration',
  },
});
