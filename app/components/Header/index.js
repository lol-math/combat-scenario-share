import React from 'react';
import { Menu, Layout } from 'antd';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import Banner from './banner.jpg';
import messages from './messages';

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        {/* <A href="https://twitter.com/mxstbr">
          <Img src={Banner} alt="react-boilerplate - Logo" />
        </A> */}
        <Layout.Header>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['home']}
            style={{ lineHeight: '64px' }}
          >
            <Menu.Item key="home">
              <Link to="/">
                <FormattedMessage {...messages.home} />
              </Link>
            </Menu.Item>
            <Menu.Item key="add">
              <Link to="/add">
                <FormattedMessage {...messages.add} />
              </Link>
            </Menu.Item>
            <Menu.Item key="login">
              <Link to="/login">
                <FormattedMessage {...messages.login} />
              </Link>
            </Menu.Item>
          </Menu>
        </Layout.Header>
        <NavBar>
        </NavBar>
      </div>
    );
  }
}

export default Header;
