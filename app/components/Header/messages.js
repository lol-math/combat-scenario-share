/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'boilerplate.components.Header.home',
    defaultMessage: 'Home',
  },
  add: {
    id: 'boilerplate.components.Header.add',
    defaultMessage: 'Add',
  },
  login: {
    id: 'boilerplate.components.Header.login',
    defaultMessage: 'Log in',
  },
});
