/**
*
* CombatScenarioSet
*
*/

import React from 'react';
import CombatScenario from '../CombatScenario';
import PropTypes from 'prop-types';
import CombatScenarioSetWrapper from './CombatScenarioSetWrapper';
// import styled from 'styled-components';


function CombatScenarioSet({ combatScenarioSet, gamePhase }) {
  return (
    <CombatScenarioSetWrapper>
      {combatScenarioSet.map((item, i) => (
        <CombatScenario key={i + gamePhase} {...item} />
      ))}

    </CombatScenarioSetWrapper>
  );
}

CombatScenarioSet.propTypes = {
  combatScenarioSet: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default CombatScenarioSet;
