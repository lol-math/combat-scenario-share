import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const NavItem = styled(Link)`
  display: inline-block;
    border-bottom: #00000000 2px solid;
    padding: 15px 25px 13px 25px;
    color: #666;
    text-decoration: none;
    transition: border-bottom-color 200ms;
    &.active, &:hover {
      color: #1890FF;
      border-bottom: #1890FF 2px solid;
    }
    &:focus {
      text-decoration: none;
    }
`;

export const NavWrapper = styled.div`
  background: #fff;
  padding: 0 25px;
`;

export const UserDisplay = styled.div`
  float: right;
  padding: 15px 25px 13px 25px;
  font-weight: bold;
  a {
    color: #666;
  }
  cursor: pointer;
`;
