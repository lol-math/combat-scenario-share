/*
 * Navigation Messages
 *
 * This contains all the text for the Navigation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Navigation.header',
    defaultMessage: 'This is the Navigation component !',
  },
  home: {
    id: 'app.components.Navigation.home',
    defaultMessage: 'Home',
  },
  add: {
    id: 'app.components.Navigation.add',
    defaultMessage: 'Add',
  },
  login: {
    id: 'app.components.Navigation.login',
    defaultMessage: 'Log in',
  },
  logout: {
    id: 'app.components.Navigation.logout',
    defaultMessage: 'Log out',
  },
  signup: {
    id: 'app.components.Navigation.signup',
    defaultMessage: 'Sign Up',
  },
  test: {
    id: 'app.components.Navigation.test',
    defaultMessage: 'TEST',
  },
});
