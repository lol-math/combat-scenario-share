/**
*
* Navigation
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { Link } from 'react-router-dom';
import { Dropdown, Menu } from 'antd';
import { FormattedMessage } from 'react-intl';
import messages from './messages';

import { NavWrapper, NavItem, UserDisplay } from './styles';
const MenuItem = Menu.Item;
const Divider = Menu.Divider;

function Navigation({ routes, username, logout, location }) {
  return (
    <NavWrapper>
      {routes.map(({ path, exact, key }) =>
        (<NavItem key={key} to={path} exact={exact} className={location.pathname === path ? 'active' : ''}>
          <FormattedMessage {...messages[key]} />
        </NavItem>)
      )}
      <Dropdown
        overlay={
          <Menu
            onClick={({ key }) => {
              switch (key) {
                case '2': logout();
                  break;
                default:
              }
            }}
          >
            <MenuItem key="1">
              <Link
                to={`/user/${username}`}
              >
                Profile
              </Link>
            </MenuItem>
            <Divider />
            <MenuItem key="2" >Log Out</MenuItem>
          </Menu>
        }
        trigger={['click']}
        placement="bottomCenter"
      >
        <UserDisplay>
          <span style={{ userSelect: 'none' }}>{username}</span>
        </UserDisplay>
      </Dropdown>
    </NavWrapper>
  );
}

Navigation.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.shape({
    path: PropTypes.string,
    key: PropTypes.string,
    exact: PropTypes.bool,
    main: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string,
      PropTypes.node,
    ]),
    hideFromMenuIfLoggedIn: PropTypes.bool,
    hideFromMenuIfNotLoggedIn: PropTypes.bool,
  })),
  username: PropTypes.string,
  logout: PropTypes.func,
};

export default Navigation;
