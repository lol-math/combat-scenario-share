/*
 * LoginForm Messages
 *
 * This contains all the text for the LoginForm component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.LoginForm.header',
    defaultMessage: 'This is the LoginForm component !',
  },
  login: {
    id: 'app.components.LoginForm.login',
    defaultMessage: 'Log in',
  },
  registerNow: {
    id: 'app.components.LoginForm.registerNow',
    defaultMessage: 'Register Now',
  },
  or: {
    id: 'app.components.LoginForm.or',
    defaultMessage: 'Or',
  },
  forgotPassword: {
    id: 'app.components.LoginForm.forgotPassword',
    defaultMessage: 'Forgot password',
  },
  rememberMe: {
    id: 'app.components.LoginForm.rememberMe',
    defaultMessage: 'Remember me',
  },
  placeholderPassword: {
    id: 'app.components.LoginForm.placeholderPassword',
    defaultMessage: 'Password',
  },
  placeholderUserName: {
    id: 'app.components.LoginForm.placeholderUserName',
    defaultMessage: 'Username',
  },
  passwordNotInputtedMessage: {
    id: 'app.components.LoginForm.passwordNotInputtedMessage',
    defaultMessage: 'Please input your password!',
  },
  usernameNotInputtedMessage: {
    id: 'app.components.LoginForm.usernameNotInputtedMessage',
    defaultMessage: 'Please input your username!',
  },
});
