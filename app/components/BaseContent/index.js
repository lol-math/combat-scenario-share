/**
*
* BaseContent
*
*/

import React from 'react';
import { Layout } from 'antd';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const PaddedContent = styled(Layout.Content)`
  padding: 25px;
`;

function BaseContent({ children }) {
  return (
    <PaddedContent>{children}</PaddedContent>
  );
}

BaseContent.propTypes = {
  children: PropTypes.node,
};

export default BaseContent;
