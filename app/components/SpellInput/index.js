/**
*
* SpellInput
*
*/

import React from 'react';
import { InputNumber } from 'antd';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const SpellInputDiv = styled.div`
  display: flex;
  /* flex: 1; */
  flex-direction: column;
  border: 1px solid #3C3C41;
  margin: 5;
  padding: 5;
  width: 64px;
  margin: 0 5px;
  padding: 3px;
  box-sizing: content-box;
`;
const AbilityTitle = styled.span`
  text-align: center;
`;

function SpellInput(props) {
  return (
    <SpellInputDiv>
      <AbilityTitle>{props.title}</AbilityTitle>
      <InputNumber
        name={props.name}
        onChange={props.onChange}
        value={props.value}
        precision={1}
        style={{ width: '64px' }}
      />

      {props.children}
    </SpellInputDiv>
  );
}

SpellInput.propTypes = {

};

export default SpellInput;
