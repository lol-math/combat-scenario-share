import Ability from './index';
export default (props) => Ability({
  ...props,
  type: 'passive',
});
