import Image from './index';
export default (props) => Image({
  ...props,
  type: 'spell',
});
