/**
*
* Ddimage
*
*/

import React from 'react';
import { Popover } from 'antd';
import dd from 'utils/dd';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ContentDiv = styled.div`
  max-width: 250px;
`;

function DdImage(props) {
  const { ability } = props;
  return (
    <Popover
      title={ability.get('name')}
      content={
        <ContentDiv dangerouslySetInnerHTML={{ __html: ability.get('description') }} />
      }
    >
      <img src={dd.images[props.type](ability.getIn(['image', 'full']))} alt={ability.name} style={{ width: props.size, height: props.size }} />
    </Popover>

  );
}

DdImage.propTypes = {
  size: PropTypes.number,
  ability: PropTypes.object,
  type: PropTypes.string,
};

export default DdImage;
