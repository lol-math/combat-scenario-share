/*
 * CombatScenarioSetListItem Messages
 *
 * This contains all the text for the CombatScenarioSetListItem component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  privateCombatScenarioPopup: {
    id: 'app.components.CombatScenarioSetListItem.privateCombatScenarioPopup',
    defaultMessage: 'This combat scenario is private',
  },
  popUpDeleteCombatScenario: {
    id: 'app.components.CombatScenarioSetListItem.popUpDeleteCombatScenario',
    defaultMessage: 'Are you sure?',
  },
  deleteButtonText: {
    id: 'app.components.CombatScenarioSetListItem.deleteButtonText',
    defaultMessage: 'Delete',
  },
});
