/**
*
* CombatScenarioSetListItem
*
*/

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { List, Avatar, Tooltip, Button, Popconfirm } from 'antd';
import dd from 'utils/dd';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import globalMessages from 'containers/App/messages';
import messages from './messages';

const ListItem = List.Item;
const ListItemMeta = List.Item.Meta;

const CombatScenarioSetListItemDescription = ({ author, dateCreated, championName, published }) =>
  (<span>{championName} - <Link to={`/user/${author}`}>{author}</Link> - {dateCreated} {!published && <Tooltip title={<FormattedMessage {...messages.privateCombatScenarioPopup} />}><span role="img" aria-label="owl">🦉</span></Tooltip>}</span>);

const getListItemActions = ({ author, currentUserName, id, onDeleteCombatScenario }) => {
  if (currentUserName === author) {
    return [
      'edit',
      <Popconfirm
        onConfirm={() => onDeleteCombatScenario(id)}
        title={<FormattedMessage {...messages.popUpDeleteCombatScenario} />}
        okText={<FormattedMessage {...globalMessages.yes} />}
        cancelText={<FormattedMessage {...globalMessages.no} />}
      >
        <Button type="danger" size="small" ><FormattedMessage {...messages.deleteButtonText} /></Button>
      </Popconfirm>,
    ];
  }
  return null;
};

function CombatScenarioSetListItem({ title, id, championImage, author, championName, dateCreated, published, currentUserName, onDeleteCombatScenario }) {
  return (
    <ListItem
      actions={getListItemActions({ author, currentUserName, id, onDeleteCombatScenario })}
    >
      <ListItemMeta
        avatar={<Avatar src={championImage ? dd.images.champion(championImage) : ''} />}
        title={<Link to={`/combatscenario/${id}`}>{title}</Link>}
        description={<CombatScenarioSetListItemDescription author={author} championName={championName} dateCreated={dateCreated} published={published} />}

      />
    </ListItem>
  );
}

CombatScenarioSetListItem.propTypes = {
  author: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  championName: PropTypes.string.isRequired,
  championImage: PropTypes.string.isRequired,
  created_at: PropTypes.string.isRequired,
};

export default CombatScenarioSetListItem;
