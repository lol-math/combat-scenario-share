export const isValidUsername = (username) => ((username.length >= 1));
export const isValidPassword = (password) => ((password.length >= 6));
